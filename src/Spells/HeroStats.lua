HeroStats = {
    _crit = {},
    _haste = {},
    _mastery = {},
    _healingTaken = {},
    _healingDone = {},
}

function HeroStats:Crit(caster, set)
    if (not self._crit[caster]) then self._crit[caster] = 0.05 end
    if (set) then self._crit[caster] = self._crit[caster] + set * 0.01 end
    -- print(self._crit[caster])
    return self._crit[caster]
end

function HeroStats:HealingTaken(caster, set)
    if (not self._healingTaken[caster]) then self._healingTaken[caster] = 1.0 end
    if (set) then self._healingTaken[caster] = self._healingTaken[caster] + set * 0.01 end
    -- print(self._crit[caster])
    return self._healingTaken[caster]
end

function HeroStats:HealingDone(caster, set)
    if (not self._healingDone[caster]) then self._healingDone[caster] = 1.0 end
    if (set) then self._healingDone[caster] = self._healingDone[caster] + set * 0.01 end
    -- print(self._crit[caster])
    return self._healingDone[caster]
end

function HeroStats.WithBonusCritDo(caster, bonus, func)
    HeroStats:Crit(caster, bonus)
    func()
    HeroStats:Crit(caster, -bonus)
end

function HeroStats:Haste(caster, set)
    if (not self._haste[caster]) then self._haste[caster] = 1.0 end
    if (set) then self._haste[caster] = self._haste[caster] + set * 0.01 end
    return self._haste[caster]
end

function HeroStats.WithBonusHasteDo(caster, bonus, func)
    HeroStats:Haste(caster, bonus)
    func()
    HeroStats:Haste(caster, -bonus)
end

function HeroStats:Mastery(caster, set)
    if (not self._mastery[caster]) then self._mastery[caster] = 0 end
    if (set) then self._mastery[caster] = self._mastery[caster] + set end
    return self._mastery[caster]
end

function HeroStats.WithBonusMasteryDo(caster, bonus, func)
    HeroStats:Mastery(caster, bonus)
    func()
    HeroStats:Mastery(caster, -bonus)
end

function HeroStats:CritChance(caster)
    local crit = HeroStats:Crit(caster)

    if (math.random() < crit) then
        return 1.5
    end
    return 1.0
end