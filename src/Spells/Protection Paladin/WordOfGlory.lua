WordOfGlory = {
    ID = {
        SPELL = SPELLS.WORD_OF_GLORY
    },

    ATTACK_TYPE = ATTACK_TYPE_MAGIC,
    DAMAGE_TYPE = DAMAGE_TYPE_MAGIC,

    Config = {},

    DefaultConfig = {
        EternalGloryChance = 0,
        TouchedBonus = 0
    }
}

function WordOfGlory.Actions()

    local caster = GetTriggerUnit()
    local target = GetSpellTargetUnit()
    local data = GetSpellConfig(caster, WordOfGlory)
    -- Bonus damage from Touched by the Light
    local amount = GetHeroInt(caster, true) + GetHeroStr(caster, true) * data.TouchedBonus

    -- If there is no Holy Power, reset ability and show error
    if (not HolyPower:Consume(caster)) then
        UnitRemoveAbility(caster, WordOfGlory.ID.SPELL)
        UnitAddAbility(caster, WordOfGlory.ID.SPELL)
        SetUnitState(caster, UNIT_STATE_MANA, GetUnitState(caster, UNIT_STATE_MANA) + BlzGetUnitAbilityManaCost(caster, WordOfGlory.ID.SPELL, 1))
        return
    end
    UnitHealTarget(caster, target, amount, true)

    if (data.EternalGloryChance and math.random() < data.EternalGloryChance) then
        HolyPower:Add(caster)
    end
end

onGlobalInit(function()
    RegisterSpellEffect(WordOfGlory.ID.SPELL, WordOfGlory.Actions)
end)