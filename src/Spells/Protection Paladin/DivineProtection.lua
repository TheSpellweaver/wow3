DivineProtection = {
    ID = {
        SPELL = SPELLS.DIVINE_PROTECTION,
        BUFF = BUFFS.DIVINE_PROTECTION,
    },
    -- SFX = MODELS.,
    ATTACK_TYPE = ATTACK_TYPE_MELEE,
    DAMAGE_TYPE = DAMAGE_TYPE_NORMAL,

    Config = {},

    DefaultConfig = {
        DamageTaken = 0.7
    }
}

function DivineProtection.Actions()
    
    local target = BlzGetEventDamageTarget()
    if (GetUnitAbilityLevel(target, DivineProtection.ID.BUFF) > 0) then
        local data = GetSpellConfig(target, DivineProtection)
        BlzSetEventDamage(GetEventDamage() * data.DamageTaken)
    end
end

onGlobalInit(function()
    local t = CreateTrigger()
    TriggerRegisterAnyUnitEventBJ(t, EVENT_PLAYER_UNIT_DAMAGING)
    TriggerAddAction(t, DivineProtection.Actions)
end)