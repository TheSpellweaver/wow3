CrusaderStrike = {
    ID = {
        SPELL = SPELLS.CRUSADER_STRIKE,
        DUMMY = SPELLS.VINDICATION,
    },
    -- SFX = MODELS.,
    ATTACK_TYPE = ATTACK_TYPE_MELEE,
    DAMAGE_TYPE = DAMAGE_TYPE_NORMAL,

    Config = {},

    DefaultConfig = {
        HolyPowerStacks = 0,
        Vindication = 0,
        GrandCrusader = 0,
        -- HealAmount = 1.5,
        -- AdvancedMending = 1.0,
    }
}

function CrusaderStrike.Actions()
    
    -- Nourish is a single target heal with cast time. When cast, its configuration is loaded for the unit
    -- (or if unit has no custom spell Config, the default one)
    local caster = GetTriggerUnit()
    local target = GetSpellTargetUnit()
    local data = GetSpellConfig(caster, CrusaderStrike)
    local damage = BlzGetUnitBaseDamage(caster, 0) * 1.2

    if (HolyPower:Consume(caster)) then
        damage = damage * 1.4
    elseif (data.HolyPowerStacks == 2) then
        HolyPower:Add(caster)
        data.HolyPowerStacks = 0
    else
        SetSpellConfig(caster, CrusaderStrike, { HolyPowerStacks = data.HolyPowerStacks + 1 })
    end

    if (data.GrandCrusader and data.GrandCrusader > 0 and math.random() <= data.GrandCrusader) then
        BlzEndUnitAbilityCooldown(caster, AvengersShield.ID.SPELL)
    end

    UnitDamageTarget(caster, target, damage, true, false, CrusaderStrike.ATTACK_TYPE, CrusaderStrike.DAMAGE_TYPE, WEAPON_TYPE_CLAW_HEAVY_SLICE)
    if (data.Vindication and data.Vindication > 0) then
        DummyCastTarget(GetOwningPlayer(caster), GetUnitX(target), GetUnitY(target), target, CrusaderStrike.ID.DUMMY, data.Vindication, "cripple")
    end
end

onGlobalInit(function()
    RegisterSpellEffect(CrusaderStrike.ID.SPELL, CrusaderStrike.Actions)
end)