Judgement = {
    ID = {
        SPELL = SPELLS.JUDGEMENT
    },

    ATTACK_TYPE = ATTACK_TYPE_MAGIC,
    DAMAGE_TYPE = DAMAGE_TYPE_MAGIC,

    Config = {},

    DefaultConfig = {
        WiseChance = 0,
        TouchedDamage = 0
    }
}

function Judgement.Actions()

    local caster = GetTriggerUnit()
    local target = GetSpellTargetUnit()
    local data = GetSpellConfig(caster, Judgement)
    -- Bonus damage from Touched by the Light
    local damage = GetHeroInt(caster, true) + GetHeroStr(caster, true) * data.TouchedDamage

    -- Judgements of the Wise
    if (data.WiseChance and math.random() < data.WiseChance) then
        HolyPower:Add(caster)
        SetUnitState(caster, UNIT_STATE_MANA, GetUnitState(caster, UNIT_STATE_MANA) + GetUnitState(caster, UNIT_STATE_MAX_MANA) * 0.15)
    end

    UnitDamageTarget(caster, target, damage, true, false, Judgement.ATTACK_TYPE, Judgement.DAMAGE_TYPE, null)
    IssueTargetOrder(target, "attack", caster)
end

onGlobalInit(function()
    RegisterSpellEffect(Judgement.ID.SPELL, Judgement.Actions)
end)