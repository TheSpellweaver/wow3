HolyPower = {
    ID = {
        AURA = AURAS.HOLY_POWER,
        BUFF = BUFFS.HOLY_POWER
    },

    Instance = { }
}

function HolyPower:Consume(caster)
    if (self.Instance[caster] and self.Instance[caster] > 0) then
        self.Instance[caster] = self.Instance[caster] - 1
        if (self.Instance[caster] == 0) then
            UnitRemoveAbility(caster, self.ID.AURA)
            UnitRemoveAbility(caster, self.ID.BUFF)
            
        end
        return true
    end
    return false
end

function HolyPower:Add(caster)
    local hp = 0
    if (self.Instance[caster]) then hp = self.Instance[caster] end
    hp = hp + 1
    if (hp == 1) then
        UnitAddAbility(caster, self.ID.AURA)
    end
    self.Instance[caster] = hp
end