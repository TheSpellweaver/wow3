AvengersShield = {
    ID = {
        SPELL = SPELLS.AVENGERS_SHIELD,
        DUMMY = DUMMIES.AVENGERS_SHIELD
    },

    ATTACK_TYPE = ATTACK_TYPE_MAGIC,
    DAMAGE_TYPE = DAMAGE_TYPE_MAGIC,

    Instance = {},
    Config = {},

    DefaultConfig = {
        WiseChance = 0,
        TouchedDamage = 0
    }
}

function AvengersShield.ThrowShield(caster, x, y, target, instance)

    if (not instance) then return end

    instance.hitUnits[target] = true
    instance.count = instance.count - 1

    TriggeredMissile.Fire(caster, x, y, target, AvengersShield.ID.DUMMY, 1, "thunderbolt", instance, function(missileCaster, data)
        -- Damage the target unit now
        UnitDamageTarget(caster, target, instance.damage, false, false, AvengersShield.ATTACK_TYPE, AvengersShield.DAMAGE_TYPE, nil)

        -- If count is > 0, then find a new target
        if (not data or data.count == 0) then return end

        FilterPlayer = GetOwningPlayer(caster)
        GroupEnumUnitsInRange(SpellGroup, GetUnitX(target), GetUnitY(target), 600, ENEMY_ALIVE_FILTER)

        local u = FirstOfGroup(SpellGroup)
        while (u and data.hitUnits[u]) do
            GroupRemoveUnit(SpellGroup, u)
            u = FirstOfGroup(SpellGroup)
        end

        if (u) then
            AvengersShield.ThrowShield(caster, GetUnitX(target), GetUnitY(target), u, data)
        end
    end)
end

function AvengersShield.Actions()

    local caster = GetTriggerUnit()
    local target = GetSpellTargetUnit()
    local data = GetSpellConfig(caster, AvengersShield)
    local damage = GetHeroStr(caster, true)

    local newInstance = {
        caster = caster,
        target = target,
        hitUnits = { target = true },
        damage = damage,
        count = 3,
    }

    AvengersShield.ThrowShield(caster, GetUnitX(caster), GetUnitY(caster), target, newInstance)
end

onGlobalInit(function()
    RegisterSpellEffect(AvengersShield.ID.SPELL, AvengersShield.Actions)
end)