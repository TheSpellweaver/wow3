Swiftmend = {
    ID = {
        SPELL = SPELLS.SWIFTMEND,
    },
    SFX = MODELS.SWIFTMEND,

    Instance = {},
    Config = {},

    DefaultConfig = {
        HealAmount = 1.5,
        AdvancedMending = 1.0,
    }
}

function Swiftmend.Actions()
    
    -- Nourish is a single target heal with cast time. When cast, its configuration is loaded for the unit
    -- (or if unit has no custom spell Config, the default one)
    local caster = GetTriggerUnit()
    local target = GetSpellTargetUnit()
    local data = GetSpellConfig(caster, Swiftmend)

    local healAmount = data.HealAmount * data.AdvancedMending

    local hasRejuv = false
    if (Rejuvenation.Instance[caster]) then
        if (Rejuvenation.Instance[caster][target]) then
            local first = Rejuvenation.Instance[caster][target]
            local last = first

            while (last) do
                if (last.duration and last.duration > 0) then
                    ReleaseTimer(last.timer)
                    Rejuvenation.SetBuff(target, false)
                    hasRejuv = true
                end
                last = last.next
            end
            Rejuvenation.Instance[caster][target] = nil
        end
    end

    if (hasRejuv) then
        UnitHealTarget(caster, target, healAmount * GetHeroInt(caster, true), true)
        DestroyEffect(AddSpecialEffectTarget(Swiftmend.SFX, target, "origin"))
    else
        SetUnitState(caster, UNIT_STATE_MANA, GetUnitState(caster, UNIT_STATE_MANA) + BlzGetAbilityManaCost(Swiftmend.ID.SPELL, 1))
        -- BlzEndUnitAbilityCooldown(caster, Swiftmend.ID.SPELL)
        UnitRemoveAbility(caster, Swiftmend.ID.SPELL)
        UnitAddAbility(caster, Swiftmend.ID.SPELL)
        SimError.Msg(GetOwningPlayer(caster), "Requires rejuvenation effect.")
    end
end

onGlobalInit(function()
    RegisterSpellEffect(Swiftmend.ID.SPELL, Swiftmend.Actions)
end)