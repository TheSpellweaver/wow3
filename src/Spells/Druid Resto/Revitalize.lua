Revitalize = {
    Instance = {},

    DefaultConfig = {
        Duration = 8.0,
    }
}

function Revitalize.Add(caster)
    local instance = Revitalize.Instance[caster]

    if (instance) then
        PauseTimer(instance.timer)
    else
        instance = {
            caster = caster
        }
        instance.timer = NewTimer(instance)
        UnitAddAbility(caster, SPELLS.REVITALIZE)
    end
    TimerStart(instance.timer, Revitalize.DefaultConfig.Duration, false, Revitalize.Remove)
    Revitalize.Instance[caster] = instance
end

function Revitalize.Remove()
    local tim = GetExpiredTimer()
    local instance = GetTimerData(tim)

    UnitRemoveAbility(instance.caster, SPELLS.REVITALIZE)
    Revitalize.Instance[instance.caster] = nil

    ReleaseTimer(tim)
end