Nourish = {
    ID = {
        SPELL = SPELLS.NOURISH,
    },
    SFX = MODELS.NOURISH,

    Instance = {},
    Config = {},

    DefaultConfig = {
        HealAmount = 0.75,
        CastTime = 2.0,
        NaturalistCTR = 0.0,
        BountyCTR = 0.0,
        -- NonInterrupt = { ORDER_NIGHTEFBUILD },
    }
}

Nourish.Heal = function(caster, target, healAmount)
    
    UnitHealTarget(caster, target, healAmount * GetHeroInt(caster, true), true)
    DestroyEffect(AddSpecialEffectTarget(Nourish.SFX, target, "origin"))
end

function Nourish.Actions()
    
    -- Nourish is a single target heal with cast time. When cast, its configuration is loaded for the unit
    -- (or if unit has no custom spell Config, the default one)
    local caster = GetTriggerUnit()
    -- if (ProgressBars.GetUnitCurrentSpell(caster) == Nourish.ID.SPELL) then return end
    local target = GetSpellTargetUnit()
    local data = GetSpellConfig(caster, Nourish)
    local castTime = (data.CastTime / HeroStats:Haste(caster) - data.NaturalistCTR)
    local healAmount = data.HealAmount

    -- 20% more healing if caster has Rejuv on target
    if (Rejuvenation.Instance[caster] and Rejuvenation.Instance[caster][target] and Rejuvenation.Instance[caster][target].duration > 0) then
        healAmount = healAmount * 1.2
    end
    if (Rejuvenation.GetCasterCount(caster) >= 3) then
        castTime = castTime * (1 - data.BountyCTR)
    end

    QueueUnitAnimation(caster, "stand ready")

    -- A ProgressBar is started for the unit which checks periodically whether the unit has finished the cast
    -- time. When it does, it executes the passed callback which in this case is a heal and a special effect.
    local pb = ProgressBars.CreateEx(caster)
    pb.z = 130.0
    pb:CastSpell(castTime, function()
        pb:Finish()
        IssueImmediateOrder(caster, "stop")
        SetUnitAnimation(caster, "spell")
        QueueUnitAnimation(caster, "stand")
        Nourish.Heal(caster, target, healAmount)
    end, Nourish.ID.SPELL)

    -- The spell is registered as Interruptable, which means if the unit is given an order while casting,
    -- Interruptable will execute the passed callback, which we use to kill the casting.
    -- So if the unit moves or recast the spell, it will cancel the spell
    Interruptable.Register(caster, function()
        -- for i, v in ipairs(data.NonInterrupt) do
        --     if (GetIssuedOrderId() == v) then return true end
        -- end
        pb:Destroy()
    end)
end

onGlobalInit(function()
    RegisterSpellCast(Nourish.ID.SPELL, Nourish.Actions)
end)