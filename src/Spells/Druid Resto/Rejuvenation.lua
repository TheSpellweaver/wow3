Rejuvenation = {
    ID = {
        SPELL = SPELLS.REJUVENATION,
    },

    UnitEffect = {},

    Instance = {},
    Config = {},

    SFX = MODELS.REJUVENATION,
    SFX2 = MODELS.REJUVENATION2,

    DefaultConfig = {
        HealAmount = 0.4,
        TickTime = 2.0,
        BaseDuration = 8.0,
        ImpRejuvDuration = 0.0,
        MaxStacks = 1,
        AdvancedMending = 1.0,
        Revitalize = 0.0,
    }
}

function Rejuvenation.SetBuff(target, bool)
    local ue = Rejuvenation.UnitEffect[target]
    if (not ue) then ue = { count = 0 } end
    
    if (bool) then
        ue.count = ue.count + 1
    else
        ue.count = ue.count - 1
    end
    DestroyEffect(ue.sfx)
    if (ue.count <= 0) then
        ue.count = 0
    elseif (ue.count == 1) then
        ue.sfx = AddSpecialEffectTarget(Rejuvenation.SFX, target, "origin")
    else
        ue.sfx = AddSpecialEffectTarget(Rejuvenation.SFX2, target, "origin")
    end
    Rejuvenation.UnitEffect[target] = ue
end

function Rejuvenation.GetCasterCount(caster)
    local casterInstances = Rejuvenation.Instance[caster]
    if (not casterInstances) then return 0 end

    local data = GetSpellConfig(caster, Rejuvenation)
    local count = 0

    local keys = {}
    for n in pairs(casterInstances) do table.insert(keys, n) end
    table.sort(keys, function(a, b) return GetHandleId(a) > GetHandleId(b) end)

    for id, target in ipairs(keys) do
        local rejuvs = casterInstances[target]
        local i = 0
        local last = { next = rejuvs }
        while (i < data.MaxStacks and last.next and last.next.duration > 0) do
            last = last.next
            i = i + 1
        end
        count = count + i
    end

    return count
end

function Rejuvenation.ReserveSlot(caster, target, instance, data)
    
    local last
    if (not Rejuvenation.Instance[caster]) then Rejuvenation.Instance[caster] = {} end
    if (Rejuvenation.Instance[caster][target]) then 
        last = Rejuvenation.Instance[caster][target] 
        instance.next = last
    end

    if (last) then
        local i = 1
        while (i < data.MaxStacks and last.next) do
            last = last.next
            i = i + 1
        end

        if (i == data.MaxStacks and last.duration > 0) then
            ReleaseTimer(last.timer)
            Rejuvenation.SetBuff(instance.target, false)
            last.next = nil
        end
    end

    Rejuvenation.Instance[caster][target] = instance
end

function Rejuvenation.Actions()
    
    local caster = GetTriggerUnit()
    local target = GetSpellTargetUnit()
    local data = GetSpellConfig(caster, Rejuvenation)

    local tickTime = (data.TickTime)/HeroStats:Haste(caster)
    local duration =  (data.BaseDuration + data.ImpRejuvDuration) / tickTime

    local instance = {
        duration = duration,
        healAmount = data.HealAmount * GetHeroInt(caster, true) * data.AdvancedMending,
        caster = caster,
        target = target
    }
    instance.timer = NewTimer(id)
    if (data.Revitalize > 0) then 
        instance.revitalize = data.Revitalize 
    end

    Rejuvenation.ReserveSlot(caster, target, instance, data)

    UnitHealTarget(caster, target, instance.healAmount, true)
    Rejuvenation.SetBuff(target, true)
    TimerStart(instance.timer, tickTime, true, function()
        local tim = GetExpiredTimer()

        UnitHealTarget(instance.caster, instance.target, instance.healAmount, true)
        instance.duration = instance.duration - 1

        -- Revitalize refresh maybe
        local rnd = math.random()
        if (instance.revitalize and rnd <= instance.revitalize) then
            Revitalize.Add(instance.caster)
        end

        if (instance.duration <= 0) then
            Rejuvenation.SetBuff(instance.target, false)
            ReleaseTimer(tim)
        end
    end)
end

onGlobalInit(function()
    RegisterSpellEffect(Rejuvenation.ID.SPELL, Rejuvenation.Actions)
end)
