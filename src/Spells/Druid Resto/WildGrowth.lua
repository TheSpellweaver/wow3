WildGrowth = {
    -- IDs
    ID = {
        SPELL = SPELLS.WILD_GROWTH,
        BUFF = BUFFS.WILD_GROWTH,
        AURA = AURAS.WILD_GROWTH
    },

    -- Ability stats
    HEAL_AMOUNT = 0.75,
    TARGET_COUNT = 4,
    RADIUS = 700.0,

    -- Technical
    HEAL_AMOUNT_DECAY = 1 / 1.10,
    HEAL_TICK_DECAY = 1.09,
    HEAL_TICK_BASE = 0.3,

    -- SFX
    SFX = "Objects\\Spawnmodels\\NightElf\\EntBirthTarget\\EntBirthTarget.mdl",

    Instance = {}
}

function WildGrowth:GetUnitPriority(u)
    local hp = GetWidgetLife(u)
    local value = hp * 0.01 + hp / GetUnitState(u, UNIT_STATE_MAX_LIFE) * 5.0

    if (GetUnitAbilityLevel(u, WildGrowth.ID.BUFF) > 0) then
        value = value * 0.65
    end
    if (GetWidgetLife(u) >= GetUnitState(u, UNIT_STATE_MAX_LIFE)*0.995) then
        value = value * 3.0
    end
    return value
end

function WildGrowth:HealTargets()
    local t = GetExpiredTimer()
    local data = GetTimerData(t) -- WildGrowth.Instance[]

    if (data.duration > 0.0) then
        data.duration = data.duration - data.tickDuration

        -- Heal the targets here
        for i,u in ipairs(data.units) do
            UnitHealTarget(data.caster, u, data.healAmount, true)
            SetTextTagLifespan(bj_lastCreatedTextTag, 0.6)
            SetTextTagFadepoint(bj_lastCreatedTextTag, 0.3)
        end

        -- Adjust new values
        data.healAmount = data.healAmount * WildGrowth.HEAL_AMOUNT_DECAY
        data.tickDuration = data.tickDuration * WildGrowth.HEAL_TICK_DECAY

        TimerStart(t, data.tickDuration, false, WildGrowth.HealTargets)
    else
        local next = next
        -- local units = data.units
        for i,u in ipairs(data.units) do
            if (not next(WildGrowth.Instance[u])) then 
                WildGrowth.SetBuff(u, false)
            end
            WildGrowth.Instance[u][data.caster] = nil
        end
        ReleaseTimer(t)
    end
end

function WildGrowth.SetBuff(target, setTo)
    if (setTo == true) then UnitAddAbility(target, WildGrowth.ID.AURA) end
    if (setTo == false) then
        UnitRemoveAbility(target, WildGrowth.ID.AURA)
        UnitRemoveAbility(target, WildGrowth.ID.BUFF)
    end
end

function WildGrowth:Actions()
    local caster = GetTriggerUnit()
    local target = GetSpellTargetUnit()
    FilterPlayer = GetOwningPlayer(caster)
    local x = GetUnitX(target)
    local y = GetUnitY(target)

    local units = TableEnumUnitsInRange(x, y, WildGrowth.RADIUS, FRIEND_ALIVE_FILTER)
    
    local priorities = {}
    for i, v in  ipairs(units) do
        table.insert(priorities, { value = WildGrowth:GetUnitPriority(v), index = i })
    end
    table.sort(priorities, function(left, right)
        return left.value < right.value
    end)

    local data = {
        units = {},
        duration = 7.0,
        caster = caster,
        healAmount = GetHeroInt(caster, true) * WildGrowth.HEAL_AMOUNT,
        tickDuration = WildGrowth.HEAL_TICK_BASE / HeroStats:Haste(caster),
        timer = NewTimer()
    }
    SetTimerData(data.timer, data)

    for i=1,WildGrowth.TARGET_COUNT do
        local u = units[priorities[i].index]
        if (u) then
            table.insert(data.units, u)

            if not WildGrowth.Instance[u] then WildGrowth.Instance[u] = {} end
            WildGrowth.Instance[u][caster] = data
            WildGrowth.SetBuff(u, true)
            DestroyEffect(AddSpecialEffectTarget(WildGrowth.SFX, u, "origin"))
        end
    end

    --TryThis(GetOwningPlayer(caster))

    TimerStart(data.timer, data.tickDuration, false, WildGrowth.HealTargets)
end

onGlobalInit(function()
    RegisterSpellEffect(WildGrowth.ID.SPELL, WildGrowth.Actions)
end)
