HotStreak = {
    ID = {
        AURA = AURAS.HOT_STREAK,
        BUFF1 = BUFFS.HEATING_UP,
        BUFF2 = BUFFS.HOT_STREAK,
    },

    SFX = {
        MODELS.HEATING_UP,
        MODELS.HOT_STREAK,
    },
    SFX_ATTACH = {
        "chest",
        "origin",
    },
    Duration = {
        3.0,
        6.0,
    },

    Instance = {},
}

function HotStreak:Consume(caster)
    if (GetUnitAbilityLevel(caster, HotStreak.ID.AURA) == 3) then
        if (HotStreak.Instance[caster]) then
            DestroyEffect(HotStreak.Instance[caster].sfx)
        end
        UnitRemoveAbility(caster, HotStreak.ID.BUFF2)
        SetUnitAbilityLevel(caster, HotStreak.ID.AURA, 1)
        return true
    end
    return false
end

function HotStreak:Add(caster)
    local hs = GetUnitAbilityLevel(caster, HotStreak.ID.AURA)
    if (hs > 0 and hs < 3) then
        local instance = {}
        if (HotStreak.Instance[caster]) then
            instance = HotStreak.Instance[caster]
            DestroyEffect(instance.sfx)
            PauseTimer(instance.timer)
        else
            instance.timer = NewTimer(instance)
        end
        UnitRemoveAbility(caster, HotStreak.ID.BUFF1)
        SetUnitAbilityLevel(caster, HotStreak.ID.AURA, hs + 1)
        instance.sfx = AddSpecialEffectTarget(HotStreak.SFX[hs], caster, HotStreak.SFX_ATTACH[hs])
        TimerStart(instance.timer, HotStreak.Duration[hs], false, function()
            SetUnitAbilityLevel(caster, HotStreak.ID.AURA, 1)
            UnitRemoveAbility(caster, HotStreak.ID.BUFF1)
            UnitRemoveAbility(caster, HotStreak.ID.BUFF2)
            DestroyEffect(instance.sfx)
        end)

        HotStreak.Instance[caster] = instance
    end
end

function HotStreak:Remove(caster)
    local hs = GetUnitAbilityLevel(caster, HotStreak.ID.AURA)
    if (hs > 0 and hs < 3) then
        SetUnitAbilityLevel(caster, HotStreak.ID.AURA, 1)
        UnitRemoveAbility(caster, HotStreak.ID.BUFF1)
    end
    if (HotStreak.Instance[caster]) then
        DestroyEffect(HotStreak.Instance[caster].sfx)
    end
end

onGlobalInit(function()
    local t = CreateTrigger()
    TriggerRegisterAnyUnitEventBJ(t, EVENT_PLAYER_UNIT_DAMAGED)
    TriggerAddAction(t, function()
        local source = GetEventDamageSource()
        
        if (BlzGetEventDamageType() == DAMAGE_TYPE_NORMAL and DamageWasCrit) then
            HotStreak:Add(source)
        end
    end)
end)