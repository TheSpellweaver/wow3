Pyroblast = {
    ID = {
        SPELL = SPELLS.PYROBLAST,
        DUMMY = DUMMIES.PYROBLAST
    },

    ATTACK_TYPE = ATTACK_TYPE_MAGIC,
    DAMAGE_TYPE = DAMAGE_TYPE_MAGIC,

    Instance = {},
    Config = {},

    DefaultConfig = {
        DamageAmount = 3.0,
        CastTime = 4.0,
        NonInterrupt = { ORDER_NIGHTELFBUILD },
        BonusMastery = 1,
    }
}


function Pyroblast.Actions()

    local caster = GetTriggerUnit()
    local target = GetSpellTargetUnit()
    local data = GetSpellConfig(caster, Pyroblast)
    local damage = GetHeroInt(caster, true) * data.DamageAmount
    local castTime = data.CastTime / HeroStats:Haste(caster)
    if (ProgressBars.GetUnitCurrentSpell(caster) == Pyroblast.ID.SPELL) then return end
    
    if (HotStreak and HotStreak:Consume(caster)) then
        
        IssueImmediateOrder(caster, "stop")
        SetUnitAnimation(caster, "spell")

        TriggeredMissile.Fire(caster, GetUnitX(caster), GetUnitY(caster), target, Pyroblast.ID.DUMMY, 1, "shadowstrike", nil, function(missileCaster, data)
            -- Damage the target unit now
            UnitDamageTarget(caster, target, damage, false, false, Pyroblast.ATTACK_TYPE, Pyroblast.DAMAGE_TYPE, nil)
            -- Apply ignite now with mastery bonus
            HeroStats.WithBonusMasteryDo(caster, (data.BonusMastery*HeroStats:Mastery(caster))+100, function()
                Ignite:Add(caster, target, DamagedAfterAmount)
            end)
            if (DamageWasCrit) then
                HotStreak:Add(caster)
            end
        end)

    else
        local pb = ProgressBars.CreateEx(caster)
        pb.z = 150.0
        pb:CastSpell(castTime, function()
            pb:Finish()

            IssueImmediateOrder(caster, "stop")
            SetUnitAnimation(caster, "spell")
            TriggeredMissile.Fire(caster, GetUnitX(caster), GetUnitY(caster), target, Pyroblast.ID.DUMMY, 1, "shadowstrike", nil, function(missileCaster, data)
                -- Damage the target unit now
                UnitDamageTarget(caster, target, damage, false, false, Pyroblast.ATTACK_TYPE, Pyroblast.DAMAGE_TYPE, nil)
                Ignite:Add(caster, target, DamagedAfterAmount)
                if (DamageWasCrit) then
                    HotStreak:Add(caster)
                end
            end)
        end, Pyroblast.ID.SPELL)
        Interruptable.Register(caster, function()
            for i, v in ipairs(data.NonInterrupt) do
                if (GetIssuedOrderId() == v) then return true end
            end
            pb:Destroy()
        end)
    end
end

onGlobalInit(function()
    RegisterSpellCast(Pyroblast.ID.SPELL, Pyroblast.Actions)
end)