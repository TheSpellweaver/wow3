FireBlast = {
    ID = {
        SPELL = SPELLS.FIRE_BLAST,
        ORDER = ORDER_BERSERK
    },

    SFX = MODELS.FIRE_CRESCENT,

    ATTACK_TYPE = ATTACK_TYPE_MAGIC,
    DAMAGE_TYPE = DAMAGE_TYPE_MAGIC,

    Instance = {},
    Config = {},

    DefaultConfig = {
        DamageAmount = 1.02,
        Range = 700.0,
        BonusCrit = 0,
    }
}

function FireBlast.Actions()

    local caster = GetTriggerUnit()
    local target = LastTarget.Get(caster)
    local data = GetSpellConfig(caster, FireBlast)
    local damage = GetHeroInt(caster, true) * data.DamageAmount
    local manaBack = 0

    if (target) then
        if (IsUnitInRange(caster, target, data.Range)) then

            HeroStats.WithBonusCritDo(caster, data.BonusCrit, function()
                UnitDamageTarget(caster, target, damage, true, false, FireBlast.ATTACK_TYPE, FireBlast.DAMAGE_TYPE, nil)
                if (GetUnitAbilityLevel(caster, Ignite.ID.SPELL) > 0) then
                    Ignite:Add(caster, target, DamagedAfterAmount)
                end
                if (DamageWasCrit) then
                    HotStreak:Add(caster)
                end
            end)
            DestroyEffect(AddSpecialEffectTarget(FireBlast.SFX, target, "chest"))
            QueueUnitAnimation(caster, "spell")
        else
            SimError.Msg(GetOwningPlayer(caster), "Target out of range.")
            manaBack = BlzGetAbilityManaCost(FireBlast.ID.SPELL, 1)
            BlzEndUnitAbilityCooldown(caster, FireBlast.ID.SPELL)
            SetUnitState(caster, UNIT_STATE_MANA, GetUnitState(caster, UNIT_STATE_MANA)+manaBack)
        end
    else
        SimError.Msg(GetOwningPlayer(caster), "There is no target.")
        manaBack = BlzGetAbilityManaCost(FireBlast.ID.SPELL, 1)
        BlzEndUnitAbilityCooldown(caster, FireBlast.ID.SPELL)
        SetUnitState(caster, UNIT_STATE_MANA, GetUnitState(caster, UNIT_STATE_MANA)+manaBack)
    end

end

onGlobalInit(function()
    RegisterSpellEnd(FireBlast.ID.SPELL, FireBlast.Actions)
end)