Scorch = {
    ID = {
        SPELL = SPELLS.SCORCH_INSTANT,
        BUFF = BUFFS.MANA_SHIELD,
    },

    SFX = MODELS.AIRSTRIKE_ROCKET,

    ATTACK_TYPE = ATTACK_TYPE_MAGIC,
    DAMAGE_TYPE = DAMAGE_TYPE_MAGIC,

    Instance = {},
    Config = {},

    DefaultConfig = {
        DamageAmount = 1.5,
        CastTime = 2.0,
        Range = 700.0,
        Firestarter = false,
        NonInterrupt = { ORDER_MANASHIELDON },
        BonusCrit = 0,
    }
}

function Scorch.Fire(caster, target, damage, critBonus)
    HeroStats.WithBonusCritDo(caster, critBonus, function()
        UnitDamageTarget(caster, target, damage, true, false, Scorch.ATTACK_TYPE, Scorch.DAMAGE_TYPE, nil)
        Ignite:Add(caster, target, DamagedAfterAmount)
        if (DamageWasCrit) then
            HotStreak:Add(caster)
        end
    end)
    DestroyEffect(AddSpecialEffectTarget(Scorch.SFX, target, "chest"))
end 

function Scorch.Actions()

    local caster = GetTriggerUnit()
    local target = LastTarget.Get(caster)
    local data = GetSpellConfig(caster, Scorch)
    local damage = GetHeroInt(caster, true) * data.DamageAmount
    local castTime = data.CastTime / HeroStats:Haste(caster)
    local manaBack = 0

    UnitRemoveAbility(caster, Scorch.ID.BUFF)
    if (ProgressBars.GetUnitCurrentSpell(caster) == Scorch.ID.SPELL) then return end
    local unitOrder = GetUnitCurrentOrder(caster)
    if (not data.Firestarter) then
        IssueImmediateOrder(caster, "stop")
    elseif (unitOrder ~= 0 and unitOrder ~= ORDER_MOVE and unitOrder ~= ORDER_SMART) then
        IssueImmediateOrder(caster, "stop")
    end

    if (target) then
        if (IsUnitInRange(caster, target, data.Range)) then

            local pb = ProgressBars.CreateEx(caster)
            pb.z = 150.0
            pb:CastSpell(castTime, function()
                pb:Finish()
                local tim = NewTimer()
                TimerStart(tim, 0.3, false, function()
                    ReleaseTimer(tim)
                    Scorch.Fire(caster, target, damage, data.BonusCrit)
                end)
                SetUnitAnimation(caster, "stand")
            end, Scorch.ID.SPELL)
            SetUnitAnimation(caster, "spell channel")
            
            Interruptable.Register(caster, function()
                local unitOrder = GetIssuedOrderId()

                if (data.Firestarter and (
                    unitOrder == ORDER_MOVE or (
                    unitOrder == ORDER_SMART))) then
                    return true
                end
                for i, v in ipairs(data.NonInterrupt) do
                    if (unitOrder == v) then return true end
                end
                pb:Destroy()
                return false
            end)
        else
            SimError.Msg(GetOwningPlayer(caster), "Target out of range.")
            manaBack = BlzGetAbilityManaCost(Scorch.ID.SPELL, 1)
            SetUnitState(caster, UNIT_STATE_MANA, GetUnitState(caster, UNIT_STATE_MANA)+manaBack)
        end
    else
        SimError.Msg(GetOwningPlayer(caster), "There is no target.")
        manaBack = BlzGetAbilityManaCost(Scorch.ID.SPELL, 1)
        SetUnitState(caster, UNIT_STATE_MANA, GetUnitState(caster, UNIT_STATE_MANA)+manaBack)
    end

end

onGlobalInit(function()
    RegisterSpellEnd(Scorch.ID.SPELL, Scorch.Actions)
end)