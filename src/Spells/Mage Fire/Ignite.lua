Ignite = {
    ID = {
        SPELL = SPELLS.IGNITE,
    },

    ATTACK_TYPE = ATTACK_TYPE_NORMAL,
    DAMAGE_TYPE = DAMAGE_TYPE_MAGIC,

    SFX = "Environment/SmallBuildingFire/SmallBuildingFire2.mdl",

    Config = {},
    Instance = {},

    DefaultConfig = {
        Duration = 9,
        DamageAmount = 0,
        Wildfire = false,
        DamageTickTime = 1.0,
        SpreadTickTime = 2.0,
        SpreadRange = 300.0,
    }
}

function Ignite:Add(caster, target, eventDamage)
    
    if (GetUnitAbilityLevel(caster, Ignite.ID.SPELL) < 1) then return end
    local data = GetSpellConfig(caster, Ignite)
    local damage = (eventDamage * data.DamageAmount) * (HeroStats:Mastery(caster)*0.01 + 1)
    local instance
    if (not self.Instance[caster]) then self.Instance[caster] = {} end

    if (self.Instance[caster][target]) then
        instance = self.Instance[caster][target]

        -- Pause the existing timer
        PauseTimer(instance.timer)
        instance.bank = instance.bank + damage
        instance.ticks = data.Duration
    else
        instance = {
            caster = caster,
            target = target,
            bank = damage,
            ticks = data.Duration,
        }
        instance.timer = NewTimer(instance)
    end

    if (data.Wildfire) then
        if (not instance.spreadTimer) then
            instance.spreadTimer = NewTimer(instance)
            TimerStart(instance.spreadTimer, data.SpreadTickTime, true, Ignite.Spread)
        end
        instance.spreadRadius = data.SpreadRange
    end
    TimerStart(instance.timer, data.DamageTickTime, true, Ignite.Damage)

    self.Instance[caster][target] = instance
    return instance
end

function Ignite.Spread()
    local tim = GetExpiredTimer()
    local instance = GetTimerData(tim)

    GroupEnumUnitsInRange(SpellGroup, GetUnitX(instance.target), GetUnitY(instance.target), instance.spreadRadius, ENEMY_ALIVE_FILTER)
    GroupRemoveUnit(SpellGroup, instance.target)

    local u = FirstOfGroup(SpellGroup)
    local selected = nil
    local inst = nil
    while (u and not selected) do
        GroupRemoveUnit(SpellGroup, u)

        -- if (u ~= instance.target) then
            inst = Ignite.Instance[instance.caster][u]
            if (inst and inst.bank < instance.bank) then selected = u
            elseif (not inst) then selected = u end
        -- end

        u = FirstOfGroup(SpellGroup)
    end

    if (selected) then
        -- KillUnit(selected)
        -- if (inst) then inst.bank = 0 end
        if (Ignite.Instance[instance.caster][selected]) then
            Ignite.Instance[instance.caster][selected].bank = 0
        end

        local newIgnite = Ignite:Add(instance.caster, selected, 0)
        newIgnite.ticks = instance.ticks
        newIgnite.bank = instance.bank
    end
end

function Ignite.Damage()
    local tim = GetExpiredTimer()
    local instance = GetTimerData(tim)

    local damageTick = instance.bank / instance.ticks
    instance.bank = instance.bank - damageTick
    instance.ticks = instance.ticks - 1

    if (instance.ticks > 0) then
        UnitDamageTarget(instance.caster, instance.target, 1+damageTick, false, false, Ignite.ATTACK_TYPE, Ignite.DAMAGE_TYPE, nil)
        DestroyEffect(AddSpecialEffectTarget(Ignite.SFX, instance.target, "head"))
    else
        ReleaseTimer(tim)
        if (instance.spreadTimer) then ReleaseTimer(instance.spreadTimer) end
        Ignite.Instance[instance.caster][instance.target] = nil
    end
end

onGlobalInit(function()
    local t = CreateTrigger()
    TriggerRegisterAnyUnitEventBJ(t, EVENT_PLAYER_UNIT_DAMAGED)
    TriggerAddAction(t, function()
        local source = GetEventDamageSource()
        local type = BlzGetEventDamageType()

        if (type == DAMAGE_TYPE_NORMAL) then
            Ignite:Add(source, BlzGetEventDamageTarget(), GetEventDamage())
        end
    end)
end)