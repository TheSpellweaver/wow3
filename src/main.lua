include("src/Config/ConfigBRC.lua")
include("src/Global/OrderId.lua")
include("src/Global/GlobalInitialization.lua")
include("src/Global/TimerUtils.lua")
include("src/Global/SpellHelper.lua")
include("src/Global/SpellEvent.lua")
include("src/Global/Interruptable.lua")
include("src/Global/DamageDisplay.lua")
include("src/Spells/HeroStats.lua")
include("src/UI/ProgressBars.lua")
include("src/Global/LastTarget.lua")
include("src/Global/SimError.lua")

-- Spells
include("src/Spells/Druid Resto/WildGrowth.lua")
include("src/Spells/Druid Resto/Nourish.lua")
include("src/Spells/Druid Resto/Rejuvenation.lua")
include("src/Spells/Druid Resto/Revitalize.lua")
include("src/Spells/Druid Resto/Swiftmend.lua")

include("src/Spells/Protection Paladin/HolyPower.lua")
include("src/Spells/Protection Paladin/CrusaderStrike.lua")
include("src/Spells/Protection Paladin/WordOfGlory.lua")
include("src/Spells/Protection Paladin/DivineProtection.lua")
include("src/Spells/Protection Paladin/Judgement.lua")
include("src/Spells/Protection Paladin/AvengersShield.lua")

include("src/Spells/Mage Fire/Pyroblast.lua")
include("src/Spells/Mage Fire/HotStreak.lua")
include("src/Spells/Mage Fire/Scorch.lua")
include("src/Spells/Mage Fire/FireBlast.lua")
include("src/Spells/Mage Fire/Ignite.lua")

-- Map Specific
include("src/MapSpecific/BrC/HeroSelect.lua")
include("src/Commands.lua")

-- Bossfights
--include("src/MapSpecific/BrC/Bossfights/GenericBossfight.lua")
--include("src/MapSpecific/BrC/Bossfights/Bonecrusher.lua")

include("src/UI/TalentViewFl.lua")
include("src/UI/TalentTreeFl.lua")
include("src/Party/PartyManager.lua")
include("src/UI/SimplePartyProfile.lua")
include("src/TalentTrees/RestoDruidTree.lua")
include("src/TalentTrees/ProtectionPaladinTree.lua")
include("src/TalentTrees/FireMageTree.lua")
-- include("src/test.lua")