PartyManager = { }

-- This class is used for managing the party of a player. Party members appear on the left as allied hero
-- Buttons. It's used for making it easier for healer to heal (and in future to select/target party members)

function PartyManager.AddMember(owner, unit)
    -- TODO: If unit is tank, make sure it gets on top.
    SetPlayerAllianceBJ( GetOwningPlayer(unit), ALLIANCE_SHARED_ADVANCED_CONTROL, true, owner)
    SetPlayerAllianceBJ( GetOwningPlayer(unit), ALLIANCE_SHARED_CONTROL, false, owner)
    MultiboardDisplay(PartyManager.mb, true)
    MultiboardDisplay(PartyManager.mb, false)
end

onGlobalInit(function()
    local t = CreateTrigger()
    for i = 0, 10 do
        TriggerRegisterPlayerChatEvent(t, Player(i), "-add", true)    
        TriggerAddAction(t, function()
            local owner = GetTriggerPlayer()
            local u = GroupPickRandomUnit(GetUnitsSelectedAll(owner))
            if (u) then PartyManager.AddMember(owner, u) end
        end)
    end
    TimerStart(NewTimer(), 0.0, false, function()
        local t = GetExpiredTimer()
        ReleaseTimer(t)
    end)
    local mb = CreateMultiboard()
    MultiboardSetRowCount(mb, 1)
    MultiboardSetColumnCount(mb, 1)
    MultiboardSetTitleText(mb, "DUMMY")
    MultiboardDisplay(mb, true)
    MultiboardDisplay(mb, false)
    PartyManager.mb = mb
end)