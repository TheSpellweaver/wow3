do
    local path = "ReplaceableTextures/CommandButtons/"
    ICONS = {
        REGENERATION = "ReplaceableTextures/CommandButtons/BTNRegenerate.blp",
        REJUVENATION = "ReplaceableTextures/CommandButtons/BTNRejuvenation.tga",
        BUTTERFLIES = path.."BTNANA_HealingButterfliesFixed.BLP",
        GREEN_BEAR_PAW = path.."BTNBear-Paw-Blue.blp",
        REVITALIZE = path.."BTNReplenish.blp",
        MARK_OF_THE_WILD = path.."BTNSpell_Nature_Regeneration.blp",
        REGROWTH = path.."BTNSpell_Nature_ResistNature.blp",
        HEALING_TOUCH = path.."BTNSpell_Nature_HealingTouch.blp",
        FUROR = path.."BTNSpell_Nature_UnyeildingStamina.blp",
        NOURISH = path.."BTNability_druid_nourish.blp",
        SWIFT_REJUVENATION = path.."BTNability_druid_empoweredrejuvination.blp",
        EMPOWERED_TOUCH = path.."BTNability_druid_empoweredtouch.blp",
        SWIFTMEND = path.."BTNinv_relics_idolofrejuvenation.blp",
        EFFLORESCENCE = path.."BTNinv_misc_herb_talandrasrose.blp",
        WILD_GROWTH = path.."BTNNatureHealingRay.blp"
    }

    SPELLS = {
        WILD_GROWTH = FourCC("A00C"),
        NOURISH = FourCC("A00J"),
        REJUVENATION = FourCC("A00D"),
        SWIFTMEND = FourCC("A01X"),
        CHAOS_BOLT = FourCC("A01L"),
        COMBUSTION = FourCC("A012"),
        CONFLAGRATE = FourCC("A01j"),
        CREATE_HEALTHSTONE = FourCC("A01T"),
        CURSE_OF_THE_ELEMENTS = FourCC("A01V"),
        ENTANGLING_ROOTS = FourCC("A00G"),
        FIRE_BLAST = FourCC("A00Y"),
        FIREBALL = FourCC("A01W"),
        FLAMESTRIKE = FourCC("A00T"),
        IMMOLATE = FourCC("A01B"),
        INCINERATE = FourCC("A01G"),
        IRONBARK = FourCC("A00F"),
        PYROBLAST = FourCC("A00X"),
        SCORCH = FourCC("A011"),
        SHADOWFURY = FourCC("A01M"),
        SOUL_FIRE = FourCC("A01D"),
        SUMMON_FELGUARD = FourCC("A01R"),
        SUMMON_IMP = FourCC("A01P"),
        TRANQUILITY = FourCC("A00I"),
        REVITALIZE = FourCC("A01Z"),
    }
    
    BUFFS = {
        WILD_GROWTH = FourCC("B001"),
    }
    
    AURAS = {
        WILD_GROWTH = FourCC("A00M"),
    }
    
    UNITS = {
        
    }

    path = "Models/"
    MODELS = {
        REJUVENATION = path.."GreenHealthSpinny20.mdl",
        REJUVENATION2 = path.."GreenHealthSpinny22.mdl",
        SWIFTMEND = path.."Green Light.mdl",
        NOURISH = path.."Healing Surge_01.mdl",
        TRANQUILITY = path.."AIreTarget_Green.mdx",
    }

end
