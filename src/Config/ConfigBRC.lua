do
    local path = "ReplaceableTextures\\CommandButtons\\"
    ICONS = {
        REGENERATION = "ReplaceableTextures\\CommandButtons\\BTNRegenerate.blp",
        REJUVENATION = "ReplaceableTextures\\CommandButtons\\BTNRejuvenation.tga",
        BUTTERFLIES = path.."BTNANA_HealingButterfliesFixed.BLP",
        GREEN_BEAR_PAW = path.."BTNBear-Paw-Blue.blp",
        REVITALIZE = path.."BTNReplenish.blp",
        MARK_OF_THE_WILD = path.."BTNSpell_Nature_Regeneration.blp",
        REGROWTH = path.."BTNSpell_Nature_ResistNature.blp",
        HEALING_TOUCH = path.."BTNSpell_Nature_HealingTouch.blp",
        FUROR = path.."BTNSpell_Nature_UnyeildingStamina.blp",
        NOURISH = path.."BTNability_druid_nourish.blp",
        SWIFT_REJUVENATION = path.."BTNability_druid_empoweredrejuvination.blp",
        EMPOWERED_TOUCH = path.."BTNability_druid_empoweredtouch.blp",
        SWIFTMEND = path.."BTNinv_relics_idolofrejuvenation.blp",
        EFFLORESCENCE = path.."BTNinv_misc_herb_talandrasrose.blp",
        WILD_GROWTH = path.."BTNNatureHealingRay.blp",
        NATURES_MAJESTY = path.."BTNAdvancedStrengthOfTheMoon.blp",

        TOUGHNESS = path.."BTNspell_holy_devotion.blp",
        CRUSADER_STRIKE = path.."BTNspell_holy_crusaderstrike.blp", -- THIS
        JUDGEMENT = path.."BTNHolyBash.blp",
        WORD_OF_GLORY = path.."BTNHeal.blp",
        VINDICATION = path.."BTNspell_holy_vindication.blp",
        AVENGERS_SHIELD = path.."BTNspell_holy_avengersshield.blp", -- THIS
        ETERNAL_GLORY = path.."BTNHealingWave.blp", 
        DIVINITY = path.."BTNspell_holy_blindingheal.blp",
        JUDGEMENTS_OF_THE_WISE  = path.."BTNSorceressMaster.blp",
        TOUCHED_BY_THE_LIGHT    = path.."BTNability_paladin_touchedbylight.blp",
        DIVINE_PROTECTION = path.."BTNHumanArmorUpThree.blp",
        DEVOTION_AURA = path.."BTNDevotion.blp",
        JUDGEMENT_ARMOR = path.."BTNHoly Paladin.blp",

        FIRE_BLAST = path.."BTNspell_fire_fireball.blp", -- Fire Blast+
        SCORCH = path.."BTNSoulBurn.blp", -- Scorch+
        PYROBLAST = path.."BTNSpell_Fire_Fireball02.blp", -- Pyroblast++
        FIRESTARTER = path.."BTNspell_fire_playingwithfire.blp", -- 
        IMPROVED_SCORCH = path.."BTNSoulBurn.blp", 
        IGNITE = path.."BTNIncinerate.blp", -- Ignite
        WILDFIRE = path.."BTNability_mage_worldinflames.blp", --
        HOT_STREAK = path.."BTNability_mage_hotstreak.blp", -- Hot Streak+
        IMPACT = path.."BTNWallOfFire.blp", -- Impact+
        CRITICAL_MASS = path.."BTNability_mage_firestarter.blp", -- Firestarter

        INCINERATE = path.."BTNIncinerateWoW.blp", -- Incinerate


        -- ../icon/Spell_Holy_Devotion.png
        -- 
    }

    SPELLS = {
        WILD_GROWTH             = FourCC("A005"),
        NOURISH     	        = FourCC("A006"),
        REJUVENATION            = FourCC("A007"),
        SWIFTMEND               = FourCC("A008"),
        CHAOS_BOLT              = FourCC("A009"),
        COMBUSTION              = FourCC("A00A"),
        CONFLAGRATE             = FourCC("A00B"),
        CREATE_HEALTHSTONE      = FourCC("A00C"),
        CURSE_OF_THE_ELEMENTS   = FourCC("A00D"),
        ENTANGLING_ROOTS        = FourCC("A00E"),
        FIRE_BLAST              = FourCC("A00F"),
        FIREBALL                = FourCC("A00G"),
        FLAMESTRIKE             = FourCC("A00H"),
        IMMOLATE                = FourCC("A00I"),
        INCINERATE              = FourCC("A00J"),
        IRONBARK                = FourCC("Ax0F"),
        SHADOWFURY              = FourCC("Ax1M"),
        SOUL_FIRE               = FourCC("Ax1D"),
        SUMMON_FELGUARD         = FourCC("Ax1R"),
        SUMMON_IMP              = FourCC("Ax1P"),
        TRANQUILITY             = FourCC("A00K"),
        REVITALIZE              = FourCC("A00L"),
        CRUSADER_STRIKE         = FourCC("A00M"),
        WORD_OF_GLORY           = FourCC("A00N"),
        JUDGEMENT               = FourCC("A00O"),
        AVENGERS_SHIELD         = FourCC("A00P"),
        DIVINE_PROTECTION       = FourCC("A00Q"),
        VINDICATION             = FourCC("A00R"),
        DEVOTION_AURA           = FourCC("A00S"),
        SCORCH                  = FourCC("A00V"),
        SCORCH_INSTANT          = FourCC("A00W"),
        PYROBLAST               = FourCC("A00X"),
        IGNITE                  = FourCC("A010"),
    }

    DUMMIES = {
        AVENGERS_SHIELD         = FourCC("A00U"),
        PYROBLAST               = FourCC("A00Y"),
    }
    
    BUFFS = {
        WILD_GROWTH             = FourCC("B000"),
        HOLY_POWER              = FourCC("B001"),
        DIVINE_PROTECTION       = FourCC("B003"),
        HOT_STREAK              = FourCC("B004"),
        HEATING_UP              = FourCC("B006"),
        MANA_SHIELD             = FourCC("BNms"),
        IGNITE                  = FourCC("B005"),
    }
    
    AURAS = {
        WILD_GROWTH             = FourCC("A012"),
        HOLY_POWER              = FourCC("A00T"),
        HOT_STREAK              = FourCC("A00Z"),
        IGNITE                  = FourCC("A011"),
    }
    
    UNITS = {
        BONECRUSHER             = FourCC('nogl'),
        CHAINS_OF_WOE           = FourCC('n002'),
    }
    
    path = "Models/"
    MODELS = {
        REJUVENATION = path.."GreenHealthSpinny20.mdl",
        REJUVENATION2 = path.."GreenHealthSpinny22.mdl",
        SWIFTMEND = path.."Green Light.mdl",
        NOURISH = path.."Healing Surge_01.mdl",
        TRANQUILITY = path.."AIreTarget_Green.mdl",
        RADIANCE = path.."Radiance Holy.mdl",
        HOT_STREAK = path.."DoomTarget_01.mdl",
        HEATING_UP = "Abilities/Spells/Orc/SpiritLink/SpiritLinkTarget.mdl",
        AIRSTRIKE_ROCKET = path.."Airstrike Rocket.mdl",
        FIRE_CRESCENT = path.."Fire Crescent Tailed.mdl",
    }

    ENEMY = Player(3)

end

