LastTarget = {
    Instance = { }
}

function LastTarget.Actions()
    local caster = GetTriggerUnit()
    local target = GetOrderTargetUnit()

    if (not LastTarget.Instance[caster]) then return end

    local instance = LastTarget.Instance[caster]
    if (instance.condition and instance.condition(caster, target)) then
        instance.target = target
    end
end

function LastTarget.Register(caster, condition)
    LastTarget.Instance[caster] = {
        target = nil,
        condition = condition
    }
end

function LastTarget.Get(caster)
    local target = nil
    if (LastTarget.Instance[caster] and LastTarget.Instance[caster].target) then
        target = LastTarget.Instance[caster].target
    end
    return target
end

onGlobalInit(function()
    local t = CreateTrigger()
    TriggerRegisterAnyUnitEventBJ(t, EVENT_PLAYER_UNIT_ISSUED_TARGET_ORDER)
    TriggerAddCondition(t, Condition(function()
        return IsUnitType(GetTriggerUnit(), UNIT_TYPE_HERO) 
    end))
    TriggerAddAction(t, LastTarget.Actions)
end)