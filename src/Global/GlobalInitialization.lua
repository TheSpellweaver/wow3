
do
    local funcs = {}
    local funcN = 0
    function onGlobalInit(func)
        funcN = funcN + 1
        funcs[funcN] = func
    end
  
    local function runUserInit()
        if funcN == 0 then return end
        local i
        for i = 1, funcN do
            funcs[i]()
            funcs[i] = nil
        end
    end
  
    local oldBliz = InitBlizzard
    function InitBlizzard()
        oldBliz()
        local oldIG = InitGlobals
        if oldIG then
            function InitGlobals()
                oldIG()
                runUserInit()
            end
        else
            runUserInit()
        end
    end
end
 