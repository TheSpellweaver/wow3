Interruptable = {
    Instance = {}
}

function Interruptable.Register(unit, callback)
    local instance = {}
    if (Interruptable.Instance[unit]) then instance = Interruptable.Instance[unit] end
    table.insert(instance, callback)
    Interruptable.Instance[unit] = instance
    return instance
end

function Interruptable.Actions()
    local unit = GetTriggerUnit()
    local instance = Interruptable.Instance[unit]
    if (not instance) then return end
    local remaining = {}

    for i, callback in ipairs(instance) do
        if (callback()) then
            table.insert(remaining, callback)
        end
    end
    Interruptable.Instance[unit] = remaining
end

do
    local t = CreateTrigger()
    TriggerRegisterAnyUnitEventBJ(t, EVENT_PLAYER_UNIT_ISSUED_ORDER)
    TriggerRegisterAnyUnitEventBJ(t, EVENT_PLAYER_UNIT_ISSUED_POINT_ORDER)
    TriggerRegisterAnyUnitEventBJ(t, EVENT_PLAYER_UNIT_ISSUED_TARGET_ORDER)
    TriggerAddCondition(t, Condition(function()
        return IsUnitType(GetTriggerUnit(), UNIT_TYPE_HERO) 
    end))
    TriggerAddAction(t, Interruptable.Actions)
end
