SimError = {
    errorSound = CreateSoundFromLabel("InterfaceError",false,false,false,10,10),
}

function SimError.Msg(forPlayer, msg)
    msg = "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n|cffffcc00"..msg.."|r"

    if (GetLocalPlayer() == forPlayer) then
        ClearTextMessages()
        DisplayTimedTextToPlayer(forPlayer, 0.52, 0.90, 2.00, msg )
        -- DisplayTimedTextFromPlayer(forPlayer, 0.52, 0.96, 2.0, msg)
        StartSound(SimError.errorSound)
    end
end
