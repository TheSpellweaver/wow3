
ENEMY_ALIVE_FILTER = Filter(function()
    local u = GetFilterUnit()
    return IsUnitEnemy(u, FilterPlayer) and GetWidgetLife(u) > 0.405
end)

FRIEND_ALIVE_FILTER = Filter(function()
    local u = GetFilterUnit()
    return IsUnitEnemy(u, FilterPlayer) == false and GetWidgetLife(u) > 0.405
end)

SpellGroup = CreateGroup()
SpellHelper = {
    TTXVEL = 0.083203125 * Cos(90*bj_DEGTORAD),
    TTYVEL = 0.083203125 * Sin(90*bj_DEGTORAD)
}

DUMMY = FourCC('nDUM')

function CheckFunc(func)
    local function on_error(err)
        print("ERROR " .. tostring(err))
    end

    return function() xpcall(func, on_error) end
end

function TableEnumUnitsInRange(x, y, radius, filter)
    local tab = {}
    GroupEnumUnitsInRange(SpellGroup, x, y, radius, filter)

    local u = FirstOfGroup(SpellGroup)
    while (u ~= nil) do
        GroupRemoveUnit(SpellGroup, u)
        table.insert(tab, u)
        u = FirstOfGroup(SpellGroup)
    end
    return tab
end

function UnitHealTarget(caster, target, amount, crits)
    local owner = GetOwningPlayer(caster)
    local size = amount / (GetHeroInt(caster, true) * 1.5 + 1) * 3 + 7
    local crit = 1.0
    local r, g, b = 5.0, 100.0, 10.0

    if (crits and HeroStats) then
        crit = HeroStats:CritChance(caster)
        if (crit > 1.0) then
            size = size * 1.5
            r, g, b = 69.0, 100.0, 1.0
        end
    end
    
    amount = amount * HeroStats:HealingTaken(target) * HeroStats:HealingDone(caster)
    SetWidgetLife(target, GetWidgetLife(target) + amount * crit)

    local s = "+".. math.floor(amount * crit)
    CreateTextTagUnitBJ(s, target, 50.00, size, r, g, b, 0)
    if (owner == GetLocalPlayer()) then
        SetTextTagVelocity(bj_lastCreatedTextTag, SpellHelper.TTXVEL, SpellHelper.TTYVEL)

        SetTextTagLifespan(bj_lastCreatedTextTag, 2.5)
        SetTextTagPermanent(bj_lastCreatedTextTag, false)
        SetTextTagFadepoint(bj_lastCreatedTextTag, 1.40)
    else
        SetTextTagVisibility(bj_lastCreatedTextTag, false)
    end
end

TriggeredMissile = {
    Instance = {}
}

function TriggeredMissile.Fire(caster, x, y, target, spellId, level, order, data, callback)
    local dummy = DummyCastTarget(GetOwningPlayer(caster), x, y, target, spellId, level, order)
    TriggeredMissile.Instance[dummy] = {
        caster = caster,
        callback = callback,
        data = data,
    }
end

onGlobalInit(function()
    local t = CreateTrigger()
    TriggerRegisterAnyUnitEventBJ(t, EVENT_PLAYER_UNIT_DAMAGED)
    TriggerAddAction(t, function()
        local source = GetEventDamageSource()
        if (TriggeredMissile.Instance[source]) then
            local data = TriggeredMissile.Instance[source]
            data.callback(data.caster, data.data)
            TriggeredMissile.Instance[source] = nil
        end
    end)
end)

function DummyCastTarget(player, x, y, target, spellId, spellLevel, order)
    local dummy = CreateUnit(player, DUMMY, x, y, 0.0)
    SetUnitX(dummy, x)
    SetUnitY(dummy, y)
    UnitAddAbility(dummy, spellId)
    SetUnitAbilityLevel(dummy, spellId, spellLevel)
    IssueTargetOrder(dummy, order, target)
    UnitApplyTimedLife(dummy, FourCC('B000'), 0.5)
    return dummy
end

function GetSpellConfig(caster, spellScope)
    if (spellScope.Config[caster]) then
        return spellScope.Config[caster]
    end
    return spellScope.DefaultConfig
end

function SetSpellConfig(caster, spellScope, data)
    if (not spellScope) then return end
    local cfg = {}
    if (spellScope.Config and spellScope.Config[caster]) then
        cfg = spellScope.Config[caster]
    end
    if (data) then
        local keys = {}
        for n in pairs(spellScope.DefaultConfig) do table.insert(keys, n) end
        for n in pairs(data) do table.insert(keys, n) end
        table.sort(keys)

        for i, key in ipairs(keys) do
            if (data[key]) then
                cfg[key] = data[key]
            elseif (not cfg[key]) then
                cfg[key] = spellScope.DefaultConfig[key]
            end
        end
    else
        cfg = nil
    end
    spellScope.Config[caster] = cfg
end

function InsertSpellConfig(caster, spellScope, data)
    if (not spellScope) then return end
    local cfg = {}

    if (spellScope.Config and spellScope.Config[caster]) then
        cfg = spellScope.Config[caster]
    end
    if (data) then
        local keys = {}
        for n in pairs(spellScope.DefaultConfig) do table.insert(keys, n) end
        for n in pairs(data) do table.insert(keys, n) end
        table.sort(keys)

        for i, key in ipairs(keys) do
            if (not cfg[key]) then
                cfg[key] = spellScope.DefaultConfig[key]
            end
            if (data[key]) then
                table.insert(cfg[key], data[key])
            end
        end
    else
        cfg = nil
    end
    spellScope.Config[caster] = cfg
end
