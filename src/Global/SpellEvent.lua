SpellCastCallbacks = {}
SpellEffectCallbacks = {}
SpellEndCallbacks = {}

function RegisterSpellCast(spellId, callback)
    SpellCastCallbacks[spellId] = callback
end

function RegisterSpellEffect(spellId, callback)
    SpellEffectCallbacks[spellId] = callback
end

function RegisterSpellEnd(spellId, callback)
    SpellEndCallbacks[spellId] = callback
end

do
    local tCast = CreateTrigger()
    TriggerRegisterAnyUnitEventBJ(tCast, EVENT_PLAYER_UNIT_SPELL_CAST)
    TriggerAddAction(tCast, function()
        if (SpellCastCallbacks[GetSpellAbilityId()]) then
            SpellCastCallbacks[GetSpellAbilityId()]()
        end
    end)

    local tEffect = CreateTrigger()
    TriggerRegisterAnyUnitEventBJ(tEffect, EVENT_PLAYER_UNIT_SPELL_EFFECT)
    TriggerAddAction(tEffect, function()
        if (SpellEffectCallbacks[GetSpellAbilityId()]) then
            SpellEffectCallbacks[GetSpellAbilityId()]()
        end
    end)

    local tEnd = CreateTrigger()
    TriggerRegisterAnyUnitEventBJ(tEnd, EVENT_PLAYER_UNIT_SPELL_ENDCAST)
    TriggerAddAction(tEnd, function()
        if (SpellEndCallbacks[GetSpellAbilityId()]) then
            SpellEndCallbacks[GetSpellAbilityId()]()
        end
    end)
end