DamageWasCrit = false
DamageWasBlocked = false
DamagedAfterAmount = -1

onGlobalInit(function()
    local t = CreateTrigger()
    TriggerRegisterAnyUnitEventBJ(t, EVENT_PLAYER_UNIT_DAMAGED)
    TriggerAddAction(t, function()

        local source = GetEventDamageSource()
        local target = BlzGetEventDamageTarget()
        local damage = GetEventDamage()
        local attackType = BlzGetEventAttackType()
        local type = BlzGetEventDamageType()

        DamagedAfterAmount = nil
        if (damage == 0) then return end

        local owner = GetOwningPlayer(source)
        -- local size = amount / (GetHeroInt(caster, true) * 1.5 + 1) * 3 + 7
        local size = (damage / 60) * 3 + 8
        local crit = 1.0

        local rgb = { 100, 100, 100 }
        if (attackType == ATTACK_TYPE_MELEE) then rgb = { 100, 100, 100 }
        elseif (attackType == ATTACK_TYPE_MAGIC) then rgb = { 25, 50, 100 } end

        if (HeroStats) then
            crit = HeroStats:CritChance(source)
            DamageWasCrit = false
            if (crit > 1.0) then
                size = size * 1.5
                damage = damage * crit
                DamageWasCrit = true
                rgb[1] = 100
                rgb[2] = rgb[2] * 0.3
                rgb[3] = rgb[2] * 0.3
            end
        end
        
        local s = math.floor(damage)
        BlzSetEventDamage(damage)
        DamagedAfterAmount = damage
        CreateTextTagUnitBJ(s, target, 50.00, size, rgb[1], rgb[2], rgb[3], 0)
        if (owner == GetLocalPlayer()) then
            SetTextTagVelocity(bj_lastCreatedTextTag, SpellHelper.TTXVEL, SpellHelper.TTYVEL)

            SetTextTagLifespan(bj_lastCreatedTextTag, 2.5)
            SetTextTagPermanent(bj_lastCreatedTextTag, false)
            SetTextTagFadepoint(bj_lastCreatedTextTag, 1.40)
        else
            SetTextTagVisibility(bj_lastCreatedTextTag, false)
        end
        
    end)
end)