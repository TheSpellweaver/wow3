function TalentTreeTemplates.CreateProtectionPaladin(unit)
    local tt = CreateTalentTree(unit)
    tt.pointsAvailable = 4
    tt.Title = "Protection Paladin Talent tree"

    -- Crusader Strike
    local crusaderStrike = tt:AddTalent(1, 0, { ID = 0 })
        :Name("Crusader Strike")
        :Description("An instant strike that deals damage based on weapon. Every third strike generates Holy Power. Consumes Holy Power for +40%% extra damage.|n|n|cffffd9b3Cast time: Instant|r|n|cffffd9b3Cooldown 2s|r")
        :Icon(ICONS.CRUSADER_STRIKE)
        :OnActivate(function(self, caster) UnitAddAbility(caster, SPELLS.CRUSADER_STRIKE) end)
        :Rank("/1")


    -- Judgement
    local judgement = tt:AddTalent(3, 0, { ID = 0 })
        :Name("Judgement")
        :Description("Strike the target with holy damage, forcing it to attack you.|n|n|cffffd9b3Cast time: Instant|nCooldown 8 sec.|nRange 250.")
        :Icon(ICONS.JUDGEMENT)
        :OnActivate(function(self, caster) UnitAddAbility(caster, SPELLS.JUDGEMENT) end)
        :Rank("/1")

    -- Word of Glory
    local wordOfGlory = tt:AddTalent(2, 1, { ID = 0 })
        :Name("Word of Glory")
        :Description("Consumes Holy Power to instantly heal the target.|n|n|cffffd9b3Cast time: Instant|nCooldown 20 sec.")
        :Icon(ICONS.WORD_OF_GLORY)
        :Rank("/1")
        :SetDependency("left", 1)
        :OnActivate(function(self, caster) UnitAddAbility(caster, SPELLS.WORD_OF_GLORY) end)

    -- Vindication
    tt:AddMultiTalent(1, 1, 2, function(i, rank)
        local bonus = 8
        return {
            Name = "Vindication",
            Description = "Your Crusader Strike reduces damage and attack speed of the target by "..(i*bonus).."%% for 30 sec.",
            Icon = ICONS.VINDICATION,
            Rank = "/2",
            Dependency = { "down", 1 },
            OnActivate = function(self, caster) SetSpellConfig(caster, CrusaderStrike, { Vindication = (rank +1) }) end
        }
    end)

    -- Improved Judgement
    tt:AddMultiTalent(3, 1, 2, function(i, rank)
        return {
            Name = "Improved Judgement",
            Description = "Increases the range of your Judgement by "..(i*150)..".",
            Icon = ICONS.JUDGEMENT,
            Rank = "/2",
            OnActivate = function(self, caster) SetUnitAbilityLevel(caster, SPELLS.JUDGEMENT, rank+2) end,
            Dependency = { "down", 1 }
        }
    end)

    -- Avenger's Shield
    local avengerShield = tt:AddTalent(1, 2, { ID = 0 })
        :Name("Avenger's Shield")
        :Description("Hurls your shield at an enemy target, dealing damage based on hero Strength and stunning them briefly. The shield bounces off to 2 additional nearby enemies.|n|n|cffffd9b3Cast time: Instant|nCooldown 24 sec.")
        :Icon(ICONS.AVENGERS_SHIELD)
        :Rank("/1")
        :SetDependency("down", 1)
        :OnActivate(function(self, caster) UnitAddAbility(caster, SPELLS.AVENGERS_SHIELD) end)
        
    -- Eternal Glory
    tt:AddMultiTalent(2, 2, 2, function(i, rank)
        return {
            Name = "Eternal Glory",
            Description = "Your Word of Glory has a "..(i*15).."%% chance not to consume Holy Power.",
            Icon = ICONS.ETERNAL_GLORY,
            Rank = "/2",
            OnActivate = function(self, caster) SetSpellConfig(caster, WordOfGlory, { EternalGloryChance = (rank*0.15 + 0.15) }) end,
            Dependency = { "down", 1 }
        }
    end)

    -- Judgements of the Wise
    tt:AddMultiTalent(3, 2, 2, function(i, rank)
        return {
            Name = "Judgements of the Wise",
            Description = "Your Judgement has "..(i*10).."%% chance to generate Holy Power and restore 15%% of your max mana.",
            Icon = ICONS.JUDGEMENTS_OF_THE_WISE,
            Rank = "/2",
            OnActivate = function(self, caster) SetSpellConfig(caster, Judgement, { WiseChance = (rank*0.1 + 0.1) }) end,
            Dependency = { "down", 1 }
        }
    end)

    -- Toughness
    tt:AddMultiTalent(0, 3, 3, function(i, rank)
        return {
            Name = "Toughness",
            Description = "Increases your armor by "..(i)..".",
            Icon = ICONS.TOUGHNESS,
            Rank = "/3",
            OnActivate = function(self, caster) BlzSetUnitArmor(caster, BlzGetUnitArmor(caster)+1) end
        }
    end)

    -- Touched by the Light
    tt:AddMultiTalent(1, 3, 3, function(i, rank)
        return {
            Name = "Touched by the Light",
            Description = "Your Judgement, Word of Glory and Consecration spells' effect is increased by "..(i*20).."%% of your Strength.",
            Icon = ICONS.TOUCHED_BY_THE_LIGHT,
            Rank = "/3",
            OnActivate = function(self, caster) 
                SetSpellConfig(caster, Judgement, { TouchedDamage = (rank*0.2 + 0.2) })
                SetSpellConfig(caster, WordOfGlory, { TouchedBonus = (rank*0.2 + 0.2) })
            end,
            Dependency = { "down", 1 }
        }
    end)

    -- Guarded by the Light

    -- Divine Protection
    local divineProtection = tt:AddTalent(3, 3, { ID = 0 })
        :Name("Divine Protection")
        :Description("Reduces damage taken for the next 8 seconds by 30%%.|n|n|cffffd9b3Cooldown 60s|r")
        :Icon(ICONS.DIVINE_PROTECTION)
        :Rank("/1")
        :SetDependency("down", 1)
        :OnActivate(function(self, caster) UnitAddAbility(caster, SPELLS.DIVINE_PROTECTION) end)

    -- Devotion Aura
    local devotionAura = tt:AddTalent(3, 4, { ID = 0 })
        :Name("Devotion Aura")
        :Description("Increases armor of nearby allies and yourself by 2.")
        :Icon(ICONS.DEVOTION_AURA)
        :Rank("/1")
        :SetDependency("down", 1)
        :OnActivate(function(self, caster) UnitAddAbility(caster, SPELLS.DEVOTION_AURA) end)

    -- Divinity
    tt:AddMultiTalent(0, 4, 3, function(i, rank)
        return {
            Name = "Divinity",
            Description = "Increases healing done by you and all healing taken by "..(i*3).."%%.",
            Icon = ICONS.DIVINITY,
            Dependency = { "down", 1 },
            Rank = "/3",
            OnActivate = function(self, caster)
                HeroStats:HealingTaken(caster, 3)
                HeroStats:HealingDone(caster, 3)
            end
        }
    end)

    -- Grand Crusader
    tt:AddMultiTalent(1, 4, 2, function(i, rank)
        return {
            Name = "Grand Crusader",
            Description = "Your Crusader Strike has a "..(i*10).."%% chance of refreshing the cooldown on your next Avenger's Shield.",
            Icon = ICONS.JUDGEMENT_ARMOR,
            Rank = "/2",
            Dependency = { "down", 1 },
            OnActivate = function(self, caster) SetSpellConfig(caster, CrusaderStrike, { GrandCrusader = (rank*0.1 + 0.1) }) end
        }
    end)

    -- -- Improved Rejuvenation
    -- tt:AddMultiTalent(1, 1, 4, function(i, rank)
    --     return {
    --         Name = "Improved Rejuvenation",
    --         Description = "Your Rejuvenation lasts for "..(i*2).." additional seconds.",
    --         Icon = ICONS.REJUVENATION,
    --         Rank = "/4",
    --         Dependency = { "down", 1 },
    --         OnActivate = function(self, caster) 
    --             SetSpellConfig(caster, Rejuvenation, { ImpRejuvDuration = (rank*2+2) })
    --         end
    --     }
    -- end)

    -- -- Advanced Mending
    -- tt:AddMultiTalent(1, 3, 3, function(i, rank)
    --     return {
    --         Name = "Advanced Mending",
    --         Description = "Increases effect of your Rejuvenation and Swiftmend spells by "..(i*5).."%%.",
    --         Icon = ICONS.SWIFT_REJUVENATION,
    --         Rank = "/3",
    --         Dependency = { {"up", 1}, {"down", 1} },
    --         OnActivate = function(self, caster) 
    --             SetSpellConfig(caster, Rejuvenation, { AdvancedMending = (rank*0.05+1.05) })
    --             SetSpellConfig(caster, Swiftmend, { AdvancedMending = (rank*0.05+1.05) })
    --         end
    --     }
    -- end)

    -- -- Revitalize
    -- tt:AddMultiTalent(1, 2, 2, function(i, rank)
    --     return {
    --         Name = "Revitalize",
    --         Description = "Has "..(i*10).."%% chance to grant:|nReplenishment - Grants nearby allies mana regeneration equal to 1% of their maximum mana for 8 sec.",
    --         Icon = ICONS.REVITALIZE,
    --         Rank = "/2",
    --         Dependency = { "down", 1 },
    --         OnActivate = function(self, caster) 
    --             SetSpellConfig(caster, Rejuvenation, { Revitalize = (rank*0.1+0.1) })
    --         end
    --     }
    -- end)

    -- -- Wild Growth
    -- local wildgrowth = tt:AddTalent(3, 4, { ID = 0 })
    --     :Name("Wild Growth")
    --     :Description("Heals up to 4 allies within 700 range of the target over 7 sec. Prioritizes healing most injured targets. The amount healed is applied quickly at first, and slows down as the Wild Growth reaches its full duration.|n|n|cffffd9b3Cast time: Instant|nCooldown 15 sec.")
    --     :Icon(ICONS.WILD_GROWTH)
    --     :OnActivate(function(self, caster) UnitAddAbility(caster, SPELLS.WILD_GROWTH) end)
    --     :Requirements(function(self, tree, caster)
    --         if (tree.pointsSpent >= 14) then return true, "" end
    --         return false, "14 points spent"
    --     end)

    return tt
end