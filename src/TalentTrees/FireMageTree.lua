function TalentTreeTemplates.CreateFireMageTree(unit)
    local tt = CreateTalentTree(unit)
    tt.pointsAvailable = 5
    tt.Title = "Fire Mage Talent tree"

    -- Fire Blast
    local fireBlast = tt:AddTalent(1, 2, { ID = 0 })
        :Name("Fire Blast")
        :Description("Instantly deal moderate fire damage to the last targeted enemy.|n|n|cffffd9b3Cast time: Instant|r|n|cffffd9b3Cooldown 6s|r")
        :Icon(ICONS.FIRE_BLAST)
        :SetDependency("down", 1)
        :OnActivate(function(self, caster) UnitAddAbility(caster, SPELLS.FIRE_BLAST) end)
        :Rank("/1")


    -- Scorch
    local scorch = tt:AddTalent(1, 0, { ID = 0 })
        :Name("Scorch")
        :Description("Scorch the last targeted enemy with fire damage.|n|n|cffffd9b3Cast time: 2 sec.")
        :Icon(ICONS.SCORCH)
        :OnActivate(function(self, caster) 
            UnitAddAbility(caster, SPELLS.SCORCH_INSTANT)
            LastTarget.Register(caster, function(caster, target)
                return IsUnitEnemy(target, GetOwningPlayer(caster))
            end)
        end)
        :Rank("/1")

    -- Pyroblast
    local pyroblast = tt:AddTalent(3, 0, { ID = 0 })
        :Name("Pyroblast")
        :Description("Hurls an immense fiery boulder that causes high Fire damage.|n|n|cffffd9b3Cast time: 4 sec.")
        :Icon(ICONS.PYROBLAST)
        :Rank("/1")
        :OnActivate(function(self, caster) UnitAddAbility(caster, SPELLS.PYROBLAST) end)

    -- Firestarter
    local firestarter = tt:AddTalent(0, 1, { ID = 0 })
        :Name("Firestarter")
        :Description("Scorch is castable while moving and attacking.")
        :Icon(ICONS.FIRESTARTER)
        :Rank("/1")
        :SetDependency("right", 3)
        :OnActivate(function(self, caster) SetSpellConfig(caster, Scorch, { Firestarter = true }) end)

    -- Improved Scorch
    tt:AddMultiTalent(1, 1, 3, function(i, rank)
        local bonus = 8
        return {
            Name = "Improved Scorch",
            Description = "Your Scorch has "..(i*10).."%% increased chance to critically strike.",
            Icon = ICONS.IMPROVED_SCORCH,
            Rank = "/3",
            Dependency = { "down", 1 },
            OnActivate = function(self, caster) SetSpellConfig(caster, Scorch, { BonusCrit = (rank*10 +10) }) end
        }
    end)

    -- Ignite
    tt:AddMultiTalent(3, 1, 3, function(i, rank)
        return {
            Name = "Ignite",
            Description = "Your target burns for an additional "..(i*15).."%% of the total direct damage caused by your autoattack, Fire Blast, Scorch, Pyroblast and Flamestrike over 9 sec. If this effect is reapplied, any remaining damage will be added to the new Ignite.",
            Icon = ICONS.IGNITE,
            Rank = "/3",
            OnActivate = function(self, caster)
                UnitAddAbility(caster, SPELLS.IGNITE)
                -- HeroStats:Mastery(caster, 10)
                SetSpellConfig(caster, Ignite, { DamageAmount = (rank*0.1+0.1)} )
            end,
            Dependency = { "down", 1 }
        }
    end)

    -- Wildfire
    local wildfire = tt:AddTalent(2, 1, { ID = 0 })
        :Name("Wildfire")
        :Description("Every 2 sec, your Ignites may spread to another nearby enemy.")
        :Icon(ICONS.WILDFIRE)
        :Rank("/1")
        :SetDependency("right", 1)
        :OnActivate(function(self, caster) 
            SetSpellConfig(caster, Ignite, { Wildfire = true }) 
        end)

    -- Hot Streak
    local hotStreak = tt:AddTalent(3, 2, { ID = 0 })
        :Name("Hot Streak")
        :Description("Two critical strikes within 3 sec will make your next Pyroblast within 6.0 sec instant cast.")
        :Icon(ICONS.HOT_STREAK)
        :Rank("/1")
        :SetDependency("down", 1)
        :OnActivate(function(self, caster) UnitAddAbility(caster, AURAS.HOT_STREAK) end)

    -- Impact
    tt:AddMultiTalent(1, 3, 4, function(i, rank)
        local bonus = 8
        return {
            Name = "Impact",
            Description = "Your Fire Blast has "..(i*25).."%% increased chance to critically strike.",
            Icon = ICONS.IMPACT,
            Rank = "/3",
            Dependency = { "down", 1 },
            OnActivate = function(self, caster) SetSpellConfig(caster, FireBlast, { BonusCrit = (rank*25+25) }) end
        }
    end)

    -- Faster Blaster
    local improvedHotStreak = tt:AddTalent(2, 3, { ID = 0 })
        :Name("Faster Blaster")
        :Description("Fire Blast is castable while moving and castable while casting other spells.")
        :Icon(ICONS.INCINERATE)
        :Rank("/1")
        :SetDependency("left", 1)
        :OnActivate(function(self, caster)
            InsertSpellConfig(caster, Scorch, { NonInterrupt = FireBlast.ID.ORDER })
            InsertSpellConfig(caster, Pyroblast, { NonInterrupt = FireBlast.ID.ORDER })

        end)

    -- Improved Hot Streak
    local improvedHotStreak = tt:AddTalent(3, 3, { ID = 0 })
        :Name("Improved Hot Streak")
        :Description("Pyroblast cast with Hot Streak! causes double the normal Ignite damage.")
        :Icon(ICONS.HOT_STREAK)
        :Rank("/1")
        :SetDependency("down", 1)
        :OnActivate(function(self, caster)
            SetSpellConfig(caster, Pyroblast, { BonusMastery = 2 })
        end)
        
    -- Critical Mass
    tt:AddMultiTalent(0, 3, 3, function(i, rank)
        return {
            Name = "Critical Mass",
            Description = "Your spells have "..(i*5).."%% increased chance to critically strike.",
            Icon = ICONS.CRITICAL_MASS,
            Rank = "/3",
            OnActivate = function(self, caster) HeroStats:Crit(caster, 5) end,
        }
    end)

    -- -- Eternal Glory
    -- tt:AddMultiTalent(2, 2, 2, function(i, rank)
    --     return {
    --         Name = "Eternal Glory",
    --         Description = "Your Word of Glory has a "..(i*15).."%% chance not to consume Holy Power.",
    --         Icon = ICONS.ETERNAL_GLORY,
    --         Rank = "/2",
    --         OnActivate = function(self, caster) SetSpellConfig(caster, WordOfGlory, { EternalGloryChance = (rank*0.15 + 0.15) }) end,
    --         Dependency = { "down", 1 }
    --     }
    -- end)

    -- -- Judgements of the Wise
    -- tt:AddMultiTalent(3, 2, 2, function(i, rank)
    --     return {
    --         Name = "Judgements of the Wise",
    --         Description = "Your Judgement has "..(i*10).."%% chance to generate Holy Power and restore 15% of your max mana.",
    --         Icon = ICONS.JUDGEMENTS_OF_THE_WISE,
    --         Rank = "/2",
    --         OnActivate = function(self, caster) SetSpellConfig(caster, Judgement, { WiseChance = (rank*0.1 + 0.1) }) end,
    --         Dependency = { "down", 1 }
    --     }
    -- end)

    -- -- Toughness
    -- tt:AddMultiTalent(0, 3, 3, function(i, rank)
    --     return {
    --         Name = "Toughness",
    --         Description = "Increases your armor by "..(i)..".",
    --         Icon = ICONS.TOUGHNESS,
    --         Rank = "/3",
    --         OnActivate = function(self, caster) BlzSetUnitArmor(caster, BlzGetUnitArmor(caster)+1) end
    --     }
    -- end)

    -- -- Touched by the Light
    -- tt:AddMultiTalent(1, 3, 3, function(i, rank)
    --     return {
    --         Name = "Touched by the Light",
    --         Description = "Your Judgement, Word of Glory and Consecration spells' effect is increased by "..(i*20).."%% of your Strength.",
    --         Icon = ICONS.TOUCHED_BY_THE_LIGHT,
    --         Rank = "/3",
    --         OnActivate = function(self, caster) 
    --             SetSpellConfig(caster, Judgement, { TouchedDamage = (rank*0.2 + 0.2) })
    --             SetSpellConfig(caster, WordOfGlory, { TouchedBonus = (rank*0.2 + 0.2) })
    --         end,
    --         Dependency = { "down", 1 }
    --     }
    -- end)

    -- -- Guarded by the Light

    -- -- Divine Protection
    -- local divineProtection = tt:AddTalent(3, 3, { ID = 0 })
    --     :Name("Divine Protection")
    --     :Description("Reduces damage taken for the next 8 seconds by 30%.|n|n|cffffd9b3Cooldown 60s|r")
    --     :Icon(ICONS.DIVINE_PROTECTION)
    --     :Rank("/1")
    --     :SetDependency("down", 1)
    --     :OnActivate(function(self, caster) UnitAddAbility(caster, SPELLS.DIVINE_PROTECTION) end)

    -- -- Devotion Aura
    -- local devotionAura = tt:AddTalent(3, 4, { ID = 0 })
    --     :Name("Devotion Aura")
    --     :Description("Increases armor of nearby allies and yourself by 2.")
    --     :Icon(ICONS.DEVOTION_AURA)
    --     :Rank("/1")
    --     :SetDependency("down", 1)
    --     :OnActivate(function(self, caster) UnitAddAbility(caster, SPELLS.DEVOTION_AURA) end)

    -- -- Divinity
    -- tt:AddMultiTalent(0, 4, 3, function(i, rank)
    --     return {
    --         Name = "Divinity",
    --         Description = "Increases healing done by you and all healing taken by "..(i*3).."%%.",
    --         Icon = ICONS.DIVINITY,
    --         Dependency = { "down", 1 },
    --         Rank = "/3",
    --         OnActivate = function(self, caster)
    --             HeroStats:HealingTaken(caster, 3)
    --             HeroStats:HealingDone(caster, 3)
    --         end
    --     }
    -- end)

    -- -- Grand Crusader
    -- tt:AddMultiTalent(1, 4, 2, function(i, rank)
    --     return {
    --         Name = "Grand Crusader",
    --         Description = "Your Crusader Strike has a "..(i*10).."%% chance of refreshing the cooldown on your next Avenger's Shield.",
    --         Icon = ICONS.JUDGEMENT_ARMOR,
    --         Rank = "/2",
    --         Dependency = { "down", 1 },
    --         OnActivate = function(self, caster) SetSpellConfig(caster, CrusaderStrike, { GrandCrusader = (rank*0.1 + 0.1) }) end
    --     }
    -- end)

    return tt
end