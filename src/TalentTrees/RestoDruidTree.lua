function TalentTreeTemplates.CreateRestoDruid(unit)
    local tt = CreateTalentTree(unit)
    tt.pointsAvailable = 4
    tt.Title = "Restoration Druid Talent tree"

    -- Rejuvenation
    local rejuvenation = tt:AddTalent(1, 0, { ID = 0 })
        :Name("Rejuvenation")
        :Description("Heals a target friendly unit over 8 seconds.|n|n|cffffd9b3Cast time: Instant")
        :Icon(ICONS.REJUVENATION)
        :OnActivate(function(self, caster) UnitAddAbility(caster, SPELLS.REJUVENATION) end)
        :Rank("/1")

    -- Nourish
    local nourish = tt:AddTalent(2, 0, { ID = 1 })
        :Name("Nourish")
        :Description("Heals a targeted unit. Heals for 20%% more if you have an active Rejuvenation on the target. |n|n|cffffd9b3Base cast time: 1.5 seconds")
        :Icon(ICONS.NOURISH)
        :OnActivate(function(self, caster) UnitAddAbility(caster, SPELLS.NOURISH) end)
        :Rank("/1")

    -- Swiftmend
    local swiftmend = tt:AddTalent(1, 4, { ID = 0 })
        :Name("Swiftmend")
        :Description("Consumes Rejuvenation effect on a friendly target to instantly heal the them.|n|n|cffffd9b3Cast time: Instant|nCooldown 15 sec.")
        :Icon(ICONS.SWIFTMEND)
        :OnActivate(function(self, caster) UnitAddAbility(caster, SPELLS.SWIFTMEND) end)
        :Requirements(function(self, tree, caster)
            if (tree.pointsSpent >= 8) then return true, "" end
            return false, "8 points spent"
        end)

    -- Naturalist
    tt:AddMultiTalent(2, 1, 3, function(i, rank)
        local bonus = 0.15
        return {
            Name = "Naturalist ",
            Description = "Decreases cast time of your Nourish spell by "..(i*bonus).." sec.",
            Icon = ICONS.HEALING_TOUCH,
            Rank = "/3",
            Dependency = { "down", 1 },
            OnActivate = function(self, caster) SetSpellConfig(caster, Nourish, { NaturalistCTR = (i*bonus+bonus) }) end
        }
    end)

    -- Nature's Bounty
    tt:AddMultiTalent(2, 2, 3, function(i, rank)
        return {
            Name = "Nature's Bounty",
            Description = "When you have Rejuvenation on three or more targets, the cast time of your Nourish spell is reduced by "..(i*10).."%%.",
            Icon = ICONS.REGROWTH,
            Rank = "/3",
            OnActivate = function(self, caster) SetSpellConfig(caster, Nourish, { BountyCTR = (rank*0.1+0.1) }) end,
            Dependency = { "down", 1 }
        }
    end)

    -- Heart of the Wild
    tt:AddMultiTalent(0, 3, 3, function(i, rank)
        return {
            Name = "Heart of the Wild",
            Description = "Increases your intellect by "..(i*2)..".",
            Icon = ICONS.BUTTERFLIES,
            Rank = "/3",
            OnActivate = function(self, caster) SetHeroInt(caster, GetHeroInt(caster)+2, true) end
        }
    end)

    -- Nature's Majesty
    tt:AddMultiTalent(0, 4, 2, function(i, rank)
        return {
            Name = "Nature's Majesty",
            Description = "Increases the critical chance with spells by "..(i*3).."%%.",
            Icon = ICONS.NATURES_MAJESTY,
            Dependency = { "down", 1 },
            Rank = "/2",
            OnActivate = function(self, caster) HeroStats:Crit(caster, 3) end
        }
    end)

    -- Furor
    tt:AddMultiTalent(0, 2, 3, function(i, rank)
        return {
            Name = "Furor",
            Description = "Increases your maximum mana by "..(i*15)..".",
            Icon = ICONS.FUROR,
            Rank = "/3",
            Dependency = { "up", 1 },
            OnActivate = function(self, caster) BlzSetUnitMaxMana(caster, BlzGetUnitMaxMana(caster) + 15) end
        }
    end)

    -- Improved Rejuvenation
    tt:AddMultiTalent(1, 1, 4, function(i, rank)
        return {
            Name = "Improved Rejuvenation",
            Description = "Your Rejuvenation lasts for "..(i*2).." additional seconds.",
            Icon = ICONS.REJUVENATION,
            Rank = "/4",
            Dependency = { "down", 1 },
            OnActivate = function(self, caster) 
                SetSpellConfig(caster, Rejuvenation, { ImpRejuvDuration = (rank*2+2) })
            end
        }
    end)

    -- Advanced Mending
    tt:AddMultiTalent(1, 3, 3, function(i, rank)
        return {
            Name = "Advanced Mending",
            Description = "Increases effect of your Rejuvenation and Swiftmend spells by "..(i*5).."%%.",
            Icon = ICONS.SWIFT_REJUVENATION,
            Rank = "/3",
            Dependency = { {"up", 1}, {"down", 1} },
            OnActivate = function(self, caster) 
                SetSpellConfig(caster, Rejuvenation, { AdvancedMending = (rank*0.05+1.05) })
                SetSpellConfig(caster, Swiftmend, { AdvancedMending = (rank*0.05+1.05) })
            end
        }
    end)

    -- Revitalize
    tt:AddMultiTalent(1, 2, 2, function(i, rank)
        return {
            Name = "Revitalize",
            Description = "Has "..(i*10).."%% chance to grant:|nReplenishment - Grants nearby allies mana regeneration equal to 1%% of their maximum mana for 8 sec.",
            Icon = ICONS.REVITALIZE,
            Rank = "/2",
            Dependency = { "down", 1 },
            OnActivate = function(self, caster) 
                SetSpellConfig(caster, Rejuvenation, { Revitalize = (rank*0.1+0.1) })
            end
        }
    end)

    -- Wild Growth
    local wildgrowth = tt:AddTalent(3, 4, { ID = 0 })
        :Name("Wild Growth")
        :Description("Heals up to 4 allies within 700 range of the target over 7 sec. Prioritizes healing most injured targets. The amount healed is applied quickly at first, and slows down as the Wild Growth reaches its full duration.|n|n|cffffd9b3Cast time: Instant|nCooldown 15 sec.")
        :Icon(ICONS.WILD_GROWTH)
        :OnActivate(function(self, caster) UnitAddAbility(caster, SPELLS.WILD_GROWTH) end)
        :Requirements(function(self, tree, caster)
            -- if (tree.pointsSpent >= 14) then return true, "" end
            -- return false, "14 points spent"
            return false, "Not yet implemented"
        end)

    return tt
end