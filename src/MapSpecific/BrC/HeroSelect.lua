PlayerHero = {}

onGlobalInit(function()
    local t = CreateTrigger()
    TriggerRegisterAnyUnitEventBJ(t, EVENT_PLAYER_UNIT_SELL)
    TriggerAddAction(t, function()
        local u = GetTriggerUnit()
        local sold = GetSoldUnit()
        local owner = GetOwningPlayer(sold)
        local id = GetUnitTypeId(sold)

        if (id == FourCC('O001')) then

            TalentView.ActiveUnit[owner] = sold
            TalentTreeTemplates.CreateProtectionPaladin(sold)
            if (owner == GetLocalPlayer()) then BlzFrameSetVisible(TalentView.Frames.viewTalentsButton, true) end

        elseif (id == FourCC('E000')) then

            TalentView.ActiveUnit[owner] = sold
            TalentTreeTemplates.CreateRestoDruid(sold)
            if (owner == GetLocalPlayer()) then BlzFrameSetVisible(TalentView.Frames.viewTalentsButton, true) end

        elseif (id == FourCC('H000')) then

            TalentView.ActiveUnit[owner] = sold
            TalentTreeTemplates.CreateFireMageTree(sold)
            if (owner == GetLocalPlayer()) then BlzFrameSetVisible(TalentView.Frames.viewTalentsButton, true) end

        end
        TalentView.ViewedUnit[owner] = nil
        if (PlayerHero[owner]) then RemoveUnit(PlayerHero[owner]) end
        PlayerHero[owner] = sold
        UnitAddType(sold, UNIT_TYPE_PEON)
    end)
end)

onGlobalInit(function()
    local t = CreateTrigger()
    TriggerRegisterAnyUnitEventBJ(t, EVENT_PLAYER_HERO_LEVEL)
    TriggerAddAction(t, function()
        local u = GetTriggerUnit()
        
        if (TalentTree[u] and TalentTree[u].pointsAvailable) then TalentTree[u].pointsAvailable = TalentTree[u].pointsAvailable + 1 end
    end)
end)