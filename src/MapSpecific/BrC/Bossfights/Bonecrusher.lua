
onGlobalInit(function()
    local bonecrusher = {
        Quake = function(diff) end,
        ChainsOfWoe = function(diff) end,
        TheSkullcracker = function(diff) end,
        WoundingStrike = function(diff) end,

        OnSetup = function(bossfight, diff) bonecrusher.boss = bossfight.Spawn(UNITS.BONECRUSHER, -1700, -2300, 0) end,
    }

    local cr = coroutine.create(function()
        if (not bonecrusher.boss) then return end

        -- Give him Wounding Strike ability
        -- Repeat Quake every x seconds

        while (GetUnitStatePercent(bossfight.boss, UNIT_STATE_LIFE, UNIT_STATE_MAX_LIFE) > 50) do
            coroutine.yield()
        end

        -- Do Chains of Woe
        print("Chains of Woe")

        while (GetWidgetLife(bonecrusher.boss) > 0.405) do
            coroutine.yield()
        end
    end)

    -- Make bossfight Start trigger, call resume on cr
end)

function RhyolithMechanics()
    while(alive...) do

        while(hp > 30%) do

            if (boss is inside arena or is doing something else) then
                DoConcussiveStomp()
                
                -- Cooldown time of Stomp and spawn mobs at 5-10s after.
                Bossfights.Cooldown(cr, 30.0)
                Bossfights.Cooldown(cr, GetRandomReal(5.0, 10.0))
                coroutine.yield()
                
                SpawnAdds()

                coroutine.yield()
            else
                -- If Stomp was unsuccessfull due to boss doing something or out of arena, retry every 1s
                Bossfights.Cooldown(cr, 1.0)
                coroutine.yield()
            end
        end
    end
end