Bossfights = {
    AI_TICK = 1.0
}

function Bossfights.Cooldown(coroutine, time)
    TimerStart(NewTimer(), time, false, function() ReleaseTimer() coroutine.resume(coroutine) end)    
end

function Bossfights.Register(bossfight)
    bossfight.units = {}
    bossfight._aiTimer = NewTimer()

    bossfight._setup = function()
        print("_setup")
        if (bossfight.OnSetup) then
            bossfight.OnSetup(bossfight)
        end
    end
    bossfight._start = function()
        print("_start")
        TimerStart(bossfight._aiTimer, Bossfights.AI_TICK, true, bossfight._ai)
        if (bossfight.OnStart) then bossfight.OnStart(bossfight) end
    end
    bossfight._ai = function()
        if (bossfight.AI) then bossfight.AI(bossfight) end

        -- Need to pass through BossActions
    end
    bossfight._end = function()
        print("_end")
        TimerStart(NewTimer(), 1.5, false, bossfight._reset)
    end
    bossfight._win = function()
        print("_win")
    end
    bossfight._reset = function()
        ReleaseTimer()
        print("_reset")
        for i, v in ipairs(bossfight.units) do
            RemoveUnit(v)
        end
        TimerStart(NewTimer(), 3.0, false, function() ReleaseTimer() bossfight._setup() end)
    end

    bossfight.Spawn = function(type, x, y, face)
        print("spawn")
        local u = CreateUnit(ENEMY, type, x, y, face)
        table.insert(bossfight.units, u)
        return u
    end
    bossfight.StartFight = function()
        bossfight._start()
    end
    bossfight.EndFight = function()
        bossfight._end()
    end

    bossfight._setup()

    table.insert(Bossfights.Instance, bossfight)
    return #Bossfights.Instance
end