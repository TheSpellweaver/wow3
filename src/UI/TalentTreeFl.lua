-------------------------------------------------------------------------------------
-- CreateTalentTree(unit) returns TalentTree[unit]

-- Base function with which you prepare a TalentTree object for a unit.
-- It's not used directly, but rather within a template that also adds
-- specific list of talents and sets their dependencies / values.

-- Make a template function called CreateMageTalentTree(unit) and call this 
-- first inside to map a base TalentTree object to the unit and start customizing it.

-------------------------------------------------------------------------------------
-- [talentTreeObject].AddTalent(self, x, y, talentData) returns Talent
-- [talentTreeObject]:AddTalent(x, y, talentData) returns Talent

-- Takes a talent tree object and prepares and adds a new Talent object on specified x/y. 
-- "talentData" is an initializer object. If it contains one of the following, 
-- they will be passed to their setter on the object before it is returned:

-- Name, Description, Icon, IconDisabled, OnActivate, OnDeactivate, Requirements

-- If talent exists on the specified position, :SetNextTalent will be called on it instead.

-------------------------------------------------------------------------------------
-- [talentTreeObject].SetNextTalent(self, x, y, talentData) returns Talent
-- [talentTreeObject]:SetNextTalent(x, y, talentData) returns Talent

-- Creates a new talent object and sets it as "next talent" to the one it's
-- called upon. When a point is gained in the base talent, it will switch
-- to the talent created by this method.

-- The method returns the NEW talent rather than old, so subsequent setters
-- will affect the NEW returned talent instead of the base one.

-------------------------------------------------------------------------------------
-- Talent Setters/Getters ------------------------------------------------------------------

-----------------------------------------------
-- .Name(self, name) or :Name(name) returns self, if name is nil returns ._name
-- This is text that is shown on top of the tooltip when hovering over the talent.

-----------------------------------------------
-- .Description(self, desc) or :Description(desc) returns self, if desc is nil returns ._desc
-- This is the text that shows under the title and Requirements.

-----------------------------------------------
-- .Icon(self, path) or :Icon(path) returns self, if path is nil returns ._icon
-- The icon that is shown for the talent. Calling this method will also set
-- the ._iconDisabled to the transformed path (added CommandButtonsDisabled/DISBTN)
-- >>> IMPORTANT: For this reason, always pass the forward slash / instead of double back \\

-----------------------------------------------
-- .IconDisabled(self, path) or :IconDisabled(path) returns self, if path is nil returns ._iconDisabled
-- This icon is shown when talent has no points in it.
-- >>> IMPORTANT: If you need icon different than enabled, always call this AFTER :Icon(path)
-- because it will get overwritten.

-----------------------------------------------
-- .OnActivate(self, callback) or :OnActivate(callback)
-- This sets a function that will get called after talent point is confirmed.
-- Use this to apply the bonus effect to the wielder of the talent tree.

-- function(self, caster) returns nothing
-- 1: [self] first parameter is always talent. You can save data to it at start and refer within callback
-- 2: [caster] second parameter will be the unit/wielder of the talent tree.

-----------------------------------------------
-- .OnDeactivate(self, callback) or :OnDeactivate(callback)
-- Currently this function is never called. But when I add ability to reset all talents,
-- This will be used to revert the changes done through onActivate.

-----------------------------------------------
-- .Requirements(self, callback) or :Requirements(callback)
-- This callback will be called when checking dependencies. If it returns false,
-- Putting points in talent will be disabled and specified text will be shown

-- function(self, caster) return isFulfilled, errorText
-- 1. [self] is the talent
-- 2. [caster] is the wielder of the Talent Tree
-- 3. [isFulfilled] is boolean. If it is not true, talent will be disabled
-- 4. [errorText] is string, it will be displayed in red as a reason

-------------------------------------------------------------------------------------

TalentTree = {}
TalentTreeTemplates = {}

function CreateTalentTree(unit)
    TalentTree[unit] = {
        unit = unit,
        pointsSpent = 0,
        pointsAvailable = 0,

        Title = "Default Talent Tree",

        Talents = {},

        TalentState = {},
        TempTalentState = nil,

        TalentLevel = {},
        TempTalentLevel = nil,

        Dependency = {},
        BaseDependency = {},

        Requirements = {},
        RequiredTalents = {},

        AddTalent = AddTalent,
        AddMultiTalent = AddMultiTalent,

        CheckDependencies = function(self, state, level, i, owner)
            
            if (not self.Talents[i].dependency) then return true, "" end
            local ok = true
            local returnString = ""
            local dep = self.Talents[i].dependency
            local depKeys = {}
            for n in pairs(dep) do table.insert(depKeys, n) end
            table.sort(depKeys)

            local tPos = { left = i - 1, up = i + TalentView.COLUMNS, right = i + 1, down = i - TalentView.COLUMNS }
            -- linkFramePos = { left = i - 1, up = i, right = i, down = i - TalentView.COLUMNS }
            local linkPos = { left = i - 1, up = i, right = i, down = i - TalentView.COLUMNS }

            for i, key in ipairs(depKeys) do
                local req = dep[key]

                -- If requirement exists, continue
                if (req) then
                    local pos = tPos[key]

                    -- Set the link visibility to enabled
                    local link
                    if (key == "left" or key == "right") then
                        link = TalentView.horizontalLink[linkPos[key]]
                    else
                        link = TalentView.verticalLink[linkPos[key]]
                    end
                    if (GetLocalPlayer() == owner) then
                        BlzFrameSetTexture(link, TalentView.ACTIVE_LINK_BOX, 0, true)
                        BlzFrameSetVisible(link, true)
                    end

                    -- Check against the state now
                    if (self.Talents[pos] and state[pos] ~= true) then
                        if (level[pos] and level[pos] < req) then
                            ok = false
                            returnString = returnString.." "..self.Talents[pos]._name.." "..req
                            if (GetLocalPlayer() == owner) then
                                BlzFrameSetTexture(link, TalentView.INACTIVE_LINK_BOX, 0, true)
                            end
                        end
                    end
                end
            end
            return ok, returnString
        end,
        CheckBaseDependency = function(self, level, i)
            local tPos = { right = i - 1, down = i + TalentView.COLUMNS, left = i + 1, up = i - TalentView.COLUMNS }
            local keys = table.sort({ "right", "down", "left", "up" })

            -- Loop through all sides (left, right, up, down)
            for i, key in ipairs(keys) do
                local pos = tPos[key]
                
                -- If a talent exists at the position at all, continue
                if (self.Talents[pos]) then

                    -- This is dependency of the talent we are looking for (left right up or down)
                    local dep = {}
                    -- If there is a BaseDependency registered from that talent (i.e. a talent point was added)
                    -- We fetch its data from the dictionary
                    if (self.BaseDependency[pos]) then dep = self.BaseDependency[pos] end

                    -- If dependency has a value/requirement towards our side, check it
                    local depValue = 0
                    if (dep[key]) then depValue = dep[key] end
                    
                    -- If the level is equal or lower than the requirement, we cannot allow reduction
                    if (level < depValue) then
                        return false
                    end
                end
            end
            return true
        end,
        ApplyTalent = function(self, i, viewedUnit)
            -- local ttl = tt.TempTalentLevel
            local talent = self.Talents[i]
    
            self.TempTalentState[i] = true
            if (self.TempTalentLevel[i]) then self.TempTalentLevel[i] = self.TempTalentLevel[i] + 1 end
            self.pointsSpent = self.pointsSpent + 1
            self.pointsAvailable = self.pointsAvailable - 1
    
            self.BaseDependency[i] = talent.dependency
            if (talent.requirements) then
                table.insert(self.RequiredTalents, talent)
            end
    
            -- On event added point
            if (talent.onPointAdded) then talent:onPointAdded(self, viewedUnit) end
    
            if (talent.nextTalent) then
                talent = talent:onNext()
            end
            return talent
        end,
    }
    return TalentTree[unit]
end

function CreateTalent(tree, position, data, x, y)
        
    local talent = {
        Name = function(self, name)
            if (name) then
                self._name = name
                self._activeName = name
                return self
            end
            return self._name
        end,
        Description = function(self, desc)
            if (desc) then
                self._desc = desc
                self._activeDesc = desc
                return self
            end 
            return self._desc 
        end,
        IconDisabled = function(self, path) 
            if (path) then
                self._iconDisabled = path 
                return self 
            end 
            return self._iconDisabled
        end,
        Icon = function(self, path)
            if (path) then
                self._icon = path
                self._activeIcon = path
                self._iconDisabled = string.gsub(path, "CommandButtons\\", "CommandButtonsDisabled\\DIS") 
                return self 
            end
            return self._icon
        end,
        OnActivate = function(self, callback) 
            if (callback) then
                self.onActivate = callback 
                return self 
            end 
            return self.onActivate 
        end,
        OnDeactivate = function(self, callback) 
            if (callback) then
                self.onDeactivate = callback 
                return self 
            end 
            return self.onDeactivate 
        end,
        SetDependency = function(self, direction, level)
            local dep = {}
            if (self.dependency) then dep = self.dependency end
            local x, y = math.floor(math.fmod((position-1), TalentView.COLUMNS)), math.floor((position-1) / TalentView.COLUMNS)
            local var = ({ left = x, right = x, up = y, down = y })[direction]
            local lower = ({ left = 1, right = 0, up = 0, down = 1 })[direction]
            local higher = ({ left = TalentView.COLUMNS, right = TalentView.COLUMNS - 1, up = TalentView.ROWS - 1, down = TalentView.ROWS })[direction]

            if (var >= lower and var < higher) then
                dep[direction] = level
                self.dependency = dep
            end
            return self
        end,
        SetNextTalent = function(self, talentData)
            
            -- Create a new talent and set their references.
            local new = CreateTalent(tree, position, talentData, x, y)
            self.nextTalent = new
            new.previousTalent = self
            table.insert(self.levels, new)
            new.levels = self.levels

            -- Set the onNext method which is called when point is added.
            self.onNext = function(self)
                -- Replace the old talent
                tree.Talents[position] = self.nextTalent
                -- Set its state as false
                tree.TempTalentState[position] = false

                -- If this talent has a dependency, set it as baseDependency to the new one.
                if (self.dependency) then
                    self.nextTalent.baseDependency = self.dependency
                end
                return self.nextTalent
            end
            return new
        end,
        SetFinalTalent = function(self, talentData)
            if (talentData.Name)          then self:ActiveName(talentData.Name) end
            if (talentData.Description)   then self:ActiveDescription(talentData.Description) end
            return self
        end,
        Requirements = function(self, callback)
            if (callback) then
                self.requirements = callback 
                return self 
            end
            return self.requirements
        end,
        RemoveRequirements = function(self, callback)
            if (callback) then
                self.removeRequirements = callback
                return self
            end
            return self.removeRequirements
        end,
        OnPointAdded = function(self, callback)
            if (callback) then
                self.onPointAdded = callback 
                return self 
            end
            return self.onPointAdded
        end,
        OnPointRemoved = function(self, callback)
            if (callback) then
                self.onPointRemoved = callback
                return self 
            end
            return self.onPointRemoved
        end,
        ActiveName = function(self, name)
            if (name) then
                self._activeName = name
                return self
            end
            return self._activeName
        end,
        ActiveDescription = function(self, desc)
            if (desc) then
                self._activeDesc = desc
                return self
            end 
            return self._activeDesc 
        end,
        Rank = function(self, rank)
            if (rank) then
                self._rank = rank
                return self
            end
            return self._rank
        end,
        SetCustom = function(self, key, value)
            if (key and value) then
                self[key] = value
            end
        end
    }
    talent.requirements = function(self, tree, caster) return true, "" end
    talent.removeRequirements = function(self, tree, caster) return true end
    talent.onPointAdded = function(self, tree, caster) return end
    talent.onPointRemoved = function(self, tree, caster) return end
    talent.onActivate = function(caster) end
    talent.onDeactivate = function(caster) end
    talent.x = x
    talent.y = y
    talent._isLink = false
    talent._rank = ""

    if (data) then
        if (data.Name)          then talent:Name(data.Name) end
        if (data.Description)   then talent:Description(data.Description) end
        if (data.Icon)          then talent:Icon(data.Icon) end
        if (data.IconDisabled)  then talent:IconDisabled(data.IconDisabled) end
        if (data.OnActivate)    then talent:OnActivate(data.OnActivate) end
        if (data.OnDeactivate)  then talent:OnDeactivate(data.OnDeactivate) end
        if (data.Requirements)  then talent:Requirements(data.Requirements) end
        if (data.RemoveRequirements)  then talent:RemoveRequirements(data.RemoveRequirements) end
        if (data.OnPointAdded)  then talent:OnPointAdded(data.OnPointAdded) end
        if (data.OnPointRemoved)      then talent:OnPointRemoved(data.OnPointRemoved) end
        if (data.SetCustom)     then talent:SetCustom(data.SetCustom[1], data.SetCustom[2]) end
        if (data.ID)            then talent.ID = data.ID end
        if (data.Rank)          then talent:Rank(data.Rank) end
        if (data.Dependency)    then 
            if (type(data.Dependency[1]) == "string" and type(data.Dependency[2]) == "number") then
                talent:SetDependency(data.Dependency[1], data.Dependency[2])
            else
                for i, v in ipairs(data.Dependency) do
                    talent:SetDependency(v[1], v[2])
                end
            end
        end
    end
    return talent
end

function AddMultiTalent(self, x, y, count, getNextLevel)
    if (count == 1) then return end
    local t = self:AddTalent(x, y, getNextLevel(1, 0))
    for i = 1, count -1 do
        local data = getNextLevel(i, i)
        data.Description = data.Description.."\n\nNext rank:\n"..getNextLevel(i+1, i+1).Description
        t = self:AddTalent(x, y, data)
    end
    local data = getNextLevel(count, count)
    t:SetFinalTalent(data)
end

function AddTalent(self, x, y, talent)

    if (x >= TalentView.COLUMNS or y >= TalentView.ROWS) then
        -- print("Error, talent position incorrect: ", x, y, "expected", TalentView.COLUMNS, TalentView.ROWS)
        return nil
    end

    local position = x + 1 + y * TalentView.COLUMNS

    -- If talent on this position already exists, call SetNextTalent instead

    if (self.Talents[position]) then
        local talents = self.Talents[position].levels
        return talents[#talents]:SetNextTalent(talent)
    end

    local t = CreateTalent(self, position, talent, x, y)
    t.levels = { t }
    self.TalentState[position] = false
    if (self.TempTalentState) then 
        self.TempTalentState[position] = false 
    end
    self.TalentLevel[position] = 0
    self.Dependency[position] = {}
    self.Talents[position] = t

    return t
end

-- Point restricted levels + dependencies
-- Bug, Down > Up, right click Up > right click Down doesn't work.
function CreateTestTalentTree(unit)
    local tt = CreateTalentTree(unit)
    local tData = {
        Name = "Create Peasant!",
        Description = "Makes a peasant for you.",
        Icon = TalentView.DEFAULT_TALENT_ICON
    }

    tt:AddTalent(0, 0, tData)
    tt:AddTalent(1, 0, tData):SetDependency("left", 1)
    tt:AddTalent(1, 1, tData):SetDependency("down", 1)
    tt:AddTalent(0, 1, tData):SetDependency("right", 1)
    tt:AddTalent(0, 0, tData):SetDependency("up", 1)
    
    tt:AddTalent(1, 0, tData):SetDependency("left", 2)
    tt:AddTalent(1, 1, tData):SetDependency("down", 2)
    tt:AddTalent(0, 1, tData):SetDependency("right", 2)
    tt:AddTalent(0, 0, tData):SetDependency("up", 2)
    
    return tt
end

-- Point dependencies with skips
function CreateTestTalentTree2(unit)
    local tt = CreateTalentTree(unit)
    local tData = {
        Name = "Create Peasant!",
        Description = "Makes a peasant for you.",
        Icon = TalentView.DEFAULT_TALENT_ICON,
        OnActivate = function(self, caster) print("Whoosh!") self.peasant = CreateUnit(GetOwningPlayer(caster), FourCC('hpea'), 0.0, 0.0, 0.0) end,
        OnDeactivate = function(self, caster) print("Boom!") KillUnit(self.peasant) end,
    }

    local link = tt:AddTalent(1, 1)
        :Name("")
        :Description("")
        :Icon("UI/Minimap/MinimapIconCreepLoc.blp")
        :SetDependency("down", 1)
        :Requirements(function() return false, "" end)
    link._isLink = true

        tt:AddTalent(1, 0, tData)
            :OnPointAdded(function(tree, caster)
                tree.TempTalentState[6] = true
            end)
            :OnPointRemoved(function(tree, caster)
                tree.TempTalentState[6] = false
            end)
        tt:AddTalent(1, 2, tData)
            :SetDependency("down", 1)
    
    return tt
end

function CreateArcaneMageTalentTree(unit)
    local tt = CreateTalentTree(unit)
    tt.points = { }
    tt.highestTier = 0

    local tier = {
        "tier",
        0
    }

    -- Arcane Concentration
    local data = {
        Name = "Arcane Concentration",
        Icon = "ReplaceableTextures/CommandButtons/BTNManaBurn.tga",
        SetCustom = tier,
        OnPointAdded = function(self, tree, caster)
            if (not tree.points[self.tier+1]) then tree.points[self.tier+1] = 0 end
            tree.points[self.tier+1] = tree.points[self.tier+1] + 1

            if (tree.highestTier < self.tier) then tree.highestTier = self.tier end
        end,
        OnPointRemoved = function(self, tree, caster)
            if (not tree.points[self.tier+1]) then tree.points[self.tier+1] = 0 end
            tree.points[self.tier+1] = tree.points[self.tier+1] - 1 
        end,
        Requirements = function(self, tree, caster)
            local sum = 0
            for i = 0, 6 do
                if (tree.points[i]) then
                    sum = sum + tree.points[i]
                end
            end
            if (sum >= self.tier * 5) then
                return true, ""
            end            
            return false, " "..(self.tier*5).." points"
        end
        
    }
    
end