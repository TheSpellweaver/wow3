ProgressBars = {
    UPDATE_POSITION_PERIOD = 0.03,
    PROGRESSBAR_MODEL = "Models/Progressbar_01.mdl",
    Instance = {}
}

function ProgressBars.Create(x, y)
    if (not x) then x = 0.0 end
    if (not y) then y = 0.0 end

    local self = {
        sfx = AddSpecialEffect(ProgressBars.PROGRESSBAR_MODEL, x, y),
        SetPercentage = ProgressBars.SetPercentage,
    }

    BlzSetSpecialEffectTime(self.sfx, 1.0)
    BlzSetSpecialEffectTimeScale(self.sfx, 0.0)

    return self
end

function ProgressBars.CreateEx(unit)
    if (not unit) then return end

    local this = {
        done = true,
        unit = unit,
        curValue = 0,
        endValue = 0,
        Finish = ProgressBars.Finish,
        Destroy = ProgressBars.Destroy,
        CastSpell = ProgressBars.CastSpell,
        spell = -1,
    }
    this.timer = NewTimer(this)
    this.sfx = AddSpecialEffect(ProgressBars.PROGRESSBAR_MODEL, GetUnitX(unit), GetUnitY(unit))
    this.SetPercentage = ProgressBars.SetPercentage

    BlzSetSpecialEffectTime(this.sfx, 1.0)
    BlzSetSpecialEffectTimeScale(this.sfx, 0.0)
    TimerStart(this.timer, ProgressBars.UPDATE_POSITION_PERIOD, true, ProgressBars.UpdatePosition)

    return this
end

function ProgressBars.SetPercentage(self, percent, speed)
    self.endValue = percent
    self.speed = speed

    self.reverse = self.curValue > self.endValue

    -- If it's not instant set
    if (speed and self.done) then
        if (not self.timer2) then self.timer2 = NewTimer(self) end

        TimerStart(self.timer2, 0.01, true, ProgressBars.UpdatePercentage)
        -- self.done = false
    else
        BlzSetSpecialEffectTime(self.sfx, percent)
    end
end

function ProgressBars.CastSpell(self, castTime, callback, spellId)
    self.endValue = 100
    self.speed = (1 / castTime)
    self.onDone = callback
    
    BlzSetSpecialEffectTime(self.sfx, 0)

    -- If it's not instant set
    if (castTime and self.done) then
        if (not self.timer2) then self.timer2 = NewTimer(self) end

        TimerStart(self.timer2, 0.01, true, ProgressBars.UpdatePercentage)
        if (spellId) then ProgressBars.Instance[self.unit] = spellId end

        if (castTime < 0.15) then DestroyEffect(self.sfx) end
    end
end

function ProgressBars.GetUnitCurrentSpell(unit)
    return ProgressBars.Instance[unit]
end

function ProgressBars.UpdatePercentage()
    local tim = GetExpiredTimer()
    local self = GetTimerData(tim)

    if (self.reverse) then

        if (self.curValue * 0.96 > self.endValue) then
            BlzSetSpecialEffectTimeScale(self.sfx, - self.speed)
            self.curValue = (self.curValue - (self.speed))
        elseif (self.curValue * 0.96 <= self.endValue) then
            PauseTimer(tim)
            BlzSetSpecialEffectTimeScale(self.sfx, 0)
            self.curValue = self.endValue
            self.done = true

            if (self.onDone) then self.onDone(self.unit) end
        end
    else
        if (self.curValue < self.endValue * 0.96) then
            BlzSetSpecialEffectTimeScale(self.sfx, self.speed)
            self.curValue = (self.curValue + (self.speed))
        elseif (self.curValue >= self.endValue * 0.96) then
            PauseTimer(tim)
            BlzSetSpecialEffectTimeScale(self.sfx, 0)
            self.curValue = self.endValue
            self.done = true

            if (self.onDone) then self.onDone(self.unit) end
        end
    end
end

function ProgressBars.UpdatePosition()
    local tim = GetExpiredTimer()
    local self = GetTimerData(tim)
    if (not self.z) then self.z = 0 end

    if (self.unit) then
        BlzSetSpecialEffectPosition(self.sfx, GetUnitX(self.unit), GetUnitY(self.unit), BlzGetUnitZ(self.unit) + self.z)
    else
        ReleaseTimer(tim)
    end
end

function ProgressBars.Finish(self)
    BlzSetSpecialEffectTimeScale(self.sfx, 3.0)
    DestroyEffect(self.sfx)
    ReleaseTimer(self.timer2)
    if (self.unit) then ProgressBars.Instance[self.unit] = nil end
end

function ProgressBars.Destroy(self)
    BlzSetSpecialEffectAlpha(self.sfx, 0)
    DestroyEffect(self.sfx)
    ReleaseTimer(self.timer2)
    if (self.unit) then ProgressBars.Instance[self.unit] = nil end
end