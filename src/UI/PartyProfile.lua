function TryThis()
    --local heroFrame = BlzGetFrameByName("StandardButtonTemplate", createContext)
    local heroBar = BlzGetOriginFrame(ORIGIN_FRAME_HERO_BAR, 0)
    -- BlzFrameSetAbsPoint(heroBar, FRAMEPOINT_TOP, 0.1, 0.1)
    BlzFrameSetSize(heroBar, 0.038, 0.37825)
    BlzFrameSetSize(heroBar, 0.038, 0.3)
    for i = 0, 5 do
        local hero = BlzGetOriginFrame(ORIGIN_FRAME_HERO_BUTTON, i)
        -- local backdrop = BlzGetFrameByName("ButtonBackdropTemplate", 0)
        -- BlzFrameClearAllPoints(hero)
        -- BlzFrameSetAbsPoint(hero, FRAMEPOINT_LEFT, 0.5, 0.55-i*0.05)
        -- THIS IS FOR DEFAULT SIZE BlzFrameSetSize(hero, 0.158, 0.038)
        BlzFrameSetSize(hero, 0.154, 0.03)
        local indi = BlzGetOriginFrame(ORIGIN_FRAME_HERO_BUTTON_INDICATOR, i)
        BlzFrameSetVisible(indi, false)
        -- BlzFrameSetSize(indi, 0.000001, 0.000001)

        local heroHp = BlzGetOriginFrame(ORIGIN_FRAME_HERO_HP_BAR, i)
        BlzFrameClearAllPoints(heroHp)
        --BlzFrameSetPoint(heroHp, FRAMEPOINT_CENTER, hero, FRAMEPOINT_CENTER, 0.0, 0.0025) -- 0.07
        BlzFrameSetPoint(heroHp, FRAMEPOINT_TOP, hero, FRAMEPOINT_TOP, -0.01, 0) -- 0.07
        BlzFrameSetPoint(heroHp, FRAMEPOINT_BOTTOM, hero, FRAMEPOINT_CENTER, 0, 0) -- 0.07
        BlzFrameSetPoint(heroHp, FRAMEPOINT_LEFT, hero, FRAMEPOINT_LEFT, 0, 0) -- 0.07
        BlzFrameSetPoint(heroHp, FRAMEPOINT_RIGHT, hero, FRAMEPOINT_RIGHT, -0.035, 0) -- 0.07
        -- BlzFrameSetSize(heroHp, 0.10, 0.016)
        
        local heroMp = BlzGetOriginFrame(ORIGIN_FRAME_HERO_MANA_BAR, i)
        BlzFrameClearAllPoints(heroMp)
        --BlzFrameSetPoint(heroMp, FRAMEPOINT_CENTER, hero, FRAMEPOINT_CENTER, 0.0, -0.012)
        BlzFrameSetPoint(heroMp, FRAMEPOINT_BOTTOM, hero, FRAMEPOINT_BOTTOM, -0.01, 0)
        BlzFrameSetPoint(heroMp, FRAMEPOINT_TOP, hero, FRAMEPOINT_CENTER, 0, 0) -- 0.07
        BlzFrameSetPoint(heroMp, FRAMEPOINT_LEFT, hero, FRAMEPOINT_LEFT, 0, 0) -- 0.07
        BlzFrameSetPoint(heroMp, FRAMEPOINT_RIGHT, hero, FRAMEPOINT_RIGHT, -0.035, 0) -- 0.07
        BlzFrameSetSize(heroMp, 0.10, 0.016)
        
        local face = BlzCreateFrameByType("BACKDROP", "Face", BlzGetOriginFrame(ORIGIN_FRAME_GAME_UI, 0), "", 0)--Create a new frame of Type BACKDROP
        -- BlzFrameSetAbsPoint(face, FRAMEPOINT_LEFT, 0.0, 0.554-i*0.05135)

        BlzFrameSetSize(face, 0.035, 0.035)
        -- THIS IS FOR DEFAULT SIZE BlzFrameSetAbsPoint(face, FRAMEPOINT_LEFT, 0.0, 0.5545-i*0.054)
        BlzFrameSetPoint(face, FRAMEPOINT_RIGHT, hero, FRAMEPOINT_RIGHT, 0, 0)
        BlzFrameSetTexture(face, "ReplaceableTextures\\CommandButtons\\BTNHeroPaladin",0, true)
    end
end

function SetupPartyProfiles()
    -- Condense the hero icons a bit
    local heroBar = BlzGetOriginFrame(ORIGIN_FRAME_HERO_BAR, 0)
    BlzFrameSetSize(heroBar, 0.038, 0.3)

    for i = 0, 11 do
        -- Fetch the hero button frame and resize it
        local hero = BlzGetOriginFrame(ORIGIN_FRAME_HERO_BUTTON, i)
        BlzFrameSetSize(hero, 0.165, 0.03) -- 0.154
        -- Remove the indicator from being seen
        local indi = BlzGetOriginFrame(ORIGIN_FRAME_HERO_BUTTON_INDICATOR, i)
        BlzFrameSetVisible(indi, false)

        local hpmp = {
            BlzGetOriginFrame(ORIGIN_FRAME_HERO_HP_BAR, i),
            BlzGetOriginFrame(ORIGIN_FRAME_HERO_MANA_BAR, i)
        }

        for i,v in ipairs(hpmp) do
            BlzFrameClearAllPoints(v)
            BlzFrameSetPoint(v, FRAMEPOINT_LEFT, hero, FRAMEPOINT_LEFT,         0, 0) -- 0.07
            BlzFrameSetPoint(v, FRAMEPOINT_RIGHT, hero, FRAMEPOINT_RIGHT,  -0.035, 0)
        end
        BlzFrameSetPoint(hpmp[1], FRAMEPOINT_TOP, hero, FRAMEPOINT_TOP,       -0.01, 0) -- 0.07
        BlzFrameSetPoint(hpmp[1], FRAMEPOINT_BOTTOM, hero, FRAMEPOINT_CENTER,     0, 0) -- 0.07

        BlzFrameSetPoint(hpmp[2], FRAMEPOINT_BOTTOM, hero, FRAMEPOINT_BOTTOM, -0.01, 0)
        BlzFrameSetPoint(hpmp[2], FRAMEPOINT_TOP, hero, FRAMEPOINT_CENTER, 0, 0) -- 0.07
        -- BlzFrameSetVisible(BlzGetFrameByName("SimpleInfoPanelIconHero", 6), false)

        if (BlzFrameIsVisible(hero)) then
            -- Create the hero icon
            -- local face = BlzCreateFrameByType("BACKDROP", "Face", BlzGetOriginFrame(ORIGIN_FRAME_GAME_UI, 0), "", 0)
            -- BlzFrameSetSize(face, 0.035, 0.035)
            -- BlzFrameSetPoint(face, FRAMEPOINT_RIGHT, hero, FRAMEPOINT_RIGHT, 0, 0)
            -- BlzFrameSetTexture(face, "ReplaceableTextures/CommandButtons/BTNHeroBloodElfPrince.tga",0, true)
        end
    end
end

onGlobalInit(function()
    -- TimerStart(NewTimer(), 0.0, false, SetupPartyProfiles)
    
end)