TalentView = {

    -- Global number of column slots on the Talent tree
    COLUMNS = 4,
    -- Global number of row slots on the Talent tree
    ROWS = 7,
    -- Width of the Talent View box
    WIDTH = 0.3,
    -- Height of the Talent View box
    HEIGHT = 0.44,
    -- Amount of empty space between Talents and the side borders
    SIDE_MARGIN = 0.1,
    -- Amount of empty space between Talents and top/bottom border
    VERTICAL_MARGIN = 0.15,

    -- Position of the Show Talents Button
    --                        Above inventory, above command card default
    SHOW_TALENTS_X = 0.14, -- 0.53             0.74               0.14
    SHOW_TALENTS_Y = 0.182, -- 0.16             0.17               0.182

    -- Global width of the Talent icons
    TALENT_WIDTH = 0.04,
    -- Global height of the Talent icons
    TALENT_HEIGHT = 0.04,

    TOOLTIP_BOX_WIDTH = 0.28,
    TOOLTIP_BOX_HEIGHT = 0.16,

    -- Selectable talent outline scale against talent width/height
    OUTLINE_BORDER_RATIO = 0.95,

    -- Whether it's possible to remove talent points by Right Clicking
    RIGHT_CLICK_TALENT_REMOVAL = false,

    -- Default icon to display while testing, not important
    DEFAULT_TALENT_ICON = "ReplaceableTextures/CommandButtons/BTNPeasant.blp",
    -- Icon for the level counter box background on bottom left of the talent
    -- UI\Console\Human\human-transport-slot.blp
    -- UI\Widgets\Console/Human/human-transport-slot.blp
    -- UI/Widgets/Console/Human/CommandButton/human-button-lvls-overlay.blp
    COUNTER_ICON_ACTIVE = "UI/Widgets/Console/Human/human-transport-slot.blp",
    COUNTER_ICON_DISABLED = "UI/WidgetsBattleNet/chaticons/iconselection-border-disabled.blp",
    
    -- ACTIVE_LINK = 'UI/Glues/SinglePlayer/Undead3D_Exp/sunwaves.blp',
    -- UI/Widgets/Console/Human/human-inventory-slotfiller.blp
    INACTIVE_LINK = 'UI/Widgets/Console/Human/human-inventory-slotfiller.blp',

    GREEN_BORDER = 'UI/Widgets/Console/Human/CommandButton/human-activebutton.blp',

    -- Texture for the Talent Dependency link
    -- Textures\star6.blp
    INACTIVE_LINK_BOX = 'UI/Widgets/Console/Human/human-inventory-slotfiller.blp',
    -- ReplaceableTextures/WorldEditUI/Doodad-Water.blp
    -- UI/Widgets/Console/Human/CommandButton/human-subgroup-background.blp
    -- Textures/star6.blp
    -- ReplaceableTextures\TeamGlow\TeamGlow00.blp
    -- ReplaceableTextures/WorldEditUI/Doodad-Water.blp'
    -- Textures\Water00.blp
    -- UI\Glues\SinglePlayer\UndeadCampaign3D\gem.blp
    -- UI/Glues/SinglePlayer/UndeadCampaign3D/gem.blp
    ACTIVE_LINK_BOX = 'Textures/Water00.blp',

    -- ====================== Internal, do not touch =================================
    ActiveUnit = {},
    ViewedUnit = {},
    verticalLink = {},
    horizontalLink = {},
    playerRightClick = {},
    playerClickedFrame = {}
    -- 
}

function TalentView:OnView(owner)
    local self = TalentView
    -- local owner = GetTriggerPlayer()

    local viewedUnit = {}
    -- If there is no active or viewed unit, we break
    if (self.ViewedUnit[owner] == nil and self.ActiveUnit[owner] == nil) then 
        return
    elseif (self.ViewedUnit[owner]) then
        viewedUnit = self.ViewedUnit[owner]
    else
        -- Now we are viewing the Active unit, so it becomes ViewedUnit instead.
        viewedUnit = self.ActiveUnit[owner]
        self.ViewedUnit[owner] = viewedUnit
    end

    -- Get the TalentTree from the unit
    local tt = TalentTree[viewedUnit]
    local tempState = {}
    local tempLevel = {}

    if (not tt) then return print("talenttree is null") end
    
    if (GetLocalPlayer() == owner) then
        -- Now that we have a viewed unit, we can render/re-render the TalentView
        -- First we disable the Show Talents button
        BlzFrameSetEnable(self.Frames.viewTalentsButton, false)
        BlzFrameSetEnable(self.Frames.viewTalentsButton, true)
        BlzFrameSetVisible(self.Frames.viewTalentsButton, false)
    end
    
    -- Check if TempTalentState exists, if not it needs to be copied
    if (tt.TempTalentState) then
        tempState = tt.TempTalentState
        tempLevel = tt.TempTalentLevel
    else
        -- Since it does not exist, it needs to be copied over
        for i = 1, self.MAX_TALENTS do
            if (tt.TalentState[i]) then
                tempState[i] = true
            else
                tempState[i] = false
            end
            if (tt.TalentLevel[i]) then tempLevel[i] = tt.TalentLevel[i] end
        end
    end

    if (GetLocalPlayer() == owner) then
        -- Hide all the links first and reset inverse Dependencies
        for i = 1, self.MAX_TALENTS do
            BlzFrameSetVisible(self.horizontalLink[i], false)
            BlzFrameSetVisible(self.verticalLink[i], false)
        end
    end

    -- Now that we have our Temporary state, either fresh or from earlier re-render,
    -- Loop through the talents and display them as necessary
    for i = 1, self.MAX_TALENTS do
        -- Fetch the Talent Frame for this index, will need it.
        local frame = TalentView.Frames.Talent[i]
        -- If talent exists, process it. If not, hide it.
        if (tt.Talents[i]) then
            local talent = tt.Talents[i]

            -- Calculate the Requirements and Dependencies
            local dep, depString, link = tt:CheckDependencies(tempState, tempLevel, i, owner)
            local req, reqString = talent:requirements(tt, viewedUnit)
            local requirements = "\n|cffff6450Requires:"..reqString.." "..depString.."|r\n\n"
            -- if (talent._name..talent._desc == "") then 
            --     BlzFrameSetEnable(frame.tooltipBox, false)
            -- end

            if (GetLocalPlayer() == owner) then
                BlzFrameSetText(frame.tooltipRank, "Rank "..tempLevel[i]..talent._rank)
                -- If both dependencies and requirements passed, the button is enabled
                if (dep and req) then
                    BlzFrameSetText(frame.tooltipText, talent._name.."\n\n"..talent._desc)
                    if (tt.pointsAvailable > 0) then
                        BlzFrameSetEnable(frame.mainButton, true)
                        BlzFrameSetVisible(frame.availableImage, true)
                    else
                        BlzFrameSetEnable(frame.mainButton, false)
                        BlzFrameSetVisible(frame.availableImage, false)
                    end
                else
                    -- If either of those failed, update the tooltip with the requirements
                    BlzFrameSetEnable(frame.mainButton, false)
                    BlzFrameSetText(frame.tooltipText, talent._name.."\n"..requirements..talent._desc)
                    BlzFrameSetVisible(frame.availableImage, false)
                end

                -- If the Talent is Active and has no levels, disable it
                if (tempState[i]) then
                    BlzFrameSetTexture(frame.mainImage, talent._icon, 0, true)
                    BlzFrameSetEnable(frame.mainButton, false)
                    BlzFrameSetVisible(frame.availableImage, false)
                    BlzFrameSetText(frame.tooltipText, talent._activeName.."\n\n"..talent._activeDesc)
                elseif (tempLevel[i] and tempLevel[i] > 0) then
                    -- If the talent under it is Active, but there's a higher level of it
                    -- which is inactive, display it enabled, but Active
                    BlzFrameSetTexture(frame.mainImage, talent._icon, 0, true)
                    -- BlzFrameSetEnable(frame.mainButton, true)
                else
                    -- If the Talent is Inactive, set it to grey icon
                    BlzFrameSetTexture(frame.mainImage, talent._iconDisabled, 0, true)
                end

                -- Need to update the level change in the counter as well
                if (tempLevel[i]) then
                    BlzFrameSetText(frame.counterText, tempLevel[i])
                    BlzFrameSetVisible(frame.counterText, true)
                    BlzFrameSetVisible(frame.counterImage, true)
                else
                    BlzFrameSetVisible(frame.counterText, false)
                    BlzFrameSetVisible(frame.counterImage, false)
                end

                -- Set the frame to visible
                if (not talent._isLink) then
                    BlzFrameSetVisible(frame.mainButton, true)
                else
                    BlzFrameSetVisible(frame.mainButton, false)
                end
            end
        elseif (GetLocalPlayer() == owner) then
            BlzFrameSetVisible(frame.mainButton, false)
            BlzFrameSetVisible(frame.availableImage, false)
        end
    end
    -- Update the global TempTalentState
    tt.TempTalentState = tempState
    tt.TempTalentLevel = tempLevel
    local titleText = tt.Title.." ("..tt.pointsAvailable.." points)"
    if (GetLocalPlayer() == owner) then
        BlzFrameSetText(self.Frames.boxTitle, titleText)
        BlzFrameSetVisible(self.Frames.box, true)
    end
end

function TalentView:OnConfirm()
    local owner = GetTriggerPlayer()

    if (not TalentView.ViewedUnit[owner]) then return end

    local viewedUnit = TalentView.ViewedUnit[owner]
    local tt = TalentTree[viewedUnit]
    local tempState = tt.TempTalentState
    local tempLevel = tt.TempTalentLevel

    for i, v in ipairs(tempState) do
        if (tt.Talents[i]) then

            if (tempLevel[i] and tt.TalentLevel[i] ~= tempLevel[i]) then
                local talents = tt.Talents[i].levels

                for j = tt.TalentLevel[i]+1, tempLevel[i] do

                    if (tempLevel[i] > tt.TalentLevel[i]) then
                        talents[j]:onActivate(viewedUnit)
                    else
                        talents[j]:onDeactivate(viewedUnit)
                    end
                end
            elseif (tt.TalentState[i] ~= nil and (tt.TalentState[i] ~= v)) then
                if (v == true) then tt.Talents[i].onActivate(viewedUnit)
                else tt.Talents[i].onDeactivate(viewedUnit) end
            end
            tt.TalentLevel[i] = tempLevel[i]
            tt.TalentState[i] = v
        end
    end

    tt.TempTalentState = nil
    TalentView.ViewedUnit[owner] = nil

    if (GetLocalPlayer() == owner) then
        BlzFrameSetVisible(TalentView.Frames.box, false)
        BlzFrameSetVisible(TalentView.Frames.viewTalentsButton, true)
    end
end

function TalentView:OnCancel()
    local owner = GetTriggerPlayer()

    if (not TalentView.ViewedUnit[owner]) then return end
    
    local viewedUnit = TalentView.ViewedUnit[owner]
    local tt = TalentTree[viewedUnit]

    -- Loop through talent choices and reduce their level to previous
    for i = 1, TalentView.MAX_TALENTS do
        if (tt.Talents[i]) then
            if (tt.TalentLevel[i] ~= tt.TempTalentLevel[i]) then
                for j = tt.TempTalentLevel[i], tt.TalentLevel[i] +1, -1 do
                    local t = tt.Talents[i].levels[j]
                    tt.Talents[i] = t
                    if (tt.Dependency[i]) then 
                        tt.Dependency[i] = t.dependency
                    end
                    if (t.onPointRemoved) then t:onPointRemoved(tt, viewedUnit) end
                    tt.pointsAvailable = tt.pointsAvailable + 1
                    tt.pointsSpent = tt.pointsSpent - 1
                end
            end
        end
    end

    -- Reset the talent state
    tt.TempTalentState = nil
    TalentView.ViewedUnit[owner] = nil

    if (GetLocalPlayer() == owner) then
        BlzFrameSetVisible(TalentView.Frames.box, false)
        BlzFrameSetVisible(TalentView.Frames.viewTalentsButton, true)

        BlzFrameSetEnable(self.Frames.cancelButton, false)
        BlzFrameSetEnable(self.Frames.cancelButton, true)
    end
end

function TalentView:OnClicked(i, owner)

    local self = TalentView
    
    local viewedUnit = {}
    -- If there is no active or viewed unit, we break
    if (self.ViewedUnit[owner] == nil) then return end
    
    viewedUnit = self.ViewedUnit[owner]
    
    local tt = TalentTree[viewedUnit]
    local state = {}

    if (tt.pointsAvailable <= 0) then return end

    -- If we clicked on the Talent whose state was false, apply it
    if (tt.TempTalentState[i] == false) then
        
        tt:ApplyTalent(i, viewedUnit)
        TalentView:OnView(owner)
    end
end

function TalentView:OnRightClick(i, owner)
    
    local self = TalentView
    
    local viewedUnit = {}
    -- If there is no active or viewed unit, we break
    if (self.ViewedUnit[owner] == nil) then return end
    viewedUnit = self.ViewedUnit[owner]
    
    local tt = TalentTree[viewedUnit]

    -- If the temporary level is higher than the actual level, we can continue
    if (tt.TalentLevel[i] < tt.TempTalentLevel[i]) then
        
        local talent = tt.Talents[i]
        local state = tt.TempTalentState[i]

        -- Check if some other talent requires this one. If it exists and
        -- if level is higher than its required level, it's okay to remove it.
        if (tt:CheckBaseDependency(tt.TempTalentLevel[i]-1, i)) then

            -- Test if removing this talent would result in violation of Requirements
            local bool = talent:removeRequirements(tt, viewedUnit)

            -- If taking point off this talent would not violate any Requirements, continue
            if (bool) then

                -- If this talent is the last in the stack, just set its state to false
                if (state == true) then
                    state = false
                    tt.TempTalentState[i] = false
                else
                    -- If it wasn't last but its temporary level is higher than its actual,
                    -- That means that it's not the First talent. We can safely replace it with lower
                    local nextLvl = tt.TempTalentLevel[i]
                    if (nextLvl > 2) then nextLvl = nextLvl - 1 end

                    talent = tt.Talents[i].levels[nextLvl]
                    -- We need to apply the new base dependency to the surroundings.
                end
                tt.Talents[i] = talent
                tt.BaseDependency[i] = talent.baseDependency
                if (tt.TempTalentLevel[i]) then tt.TempTalentLevel[i] = tt.TempTalentLevel[i] - 1 end
                if (tt.Talents[i].onPointRemoved) then tt.Talents[i]:onPointRemoved(tt, viewedUnit) end
                
                tt.pointsAvailable = tt.pointsAvailable + 1
                tt.pointsSpent = tt.pointsSpent - 1
            end
        end
    end
    self:OnView(owner)
end

function TalentFrameInitialize(i)
    
    local self = TalentView
    local tf = { Frames = {} }

    tf.clickTrigger = CreateTrigger()

    if (math.fmod(i, self.COLUMNS) < 3) then
        tf.horizontalLink = BlzCreateFrameByType("BACKDROP", "HorizontalLink", self.Frames.box, "", 0)
        self.horizontalLink[i+1] = tf.horizontalLink
    end
    if (i < (self.MAX_TALENTS - self.COLUMNS)) then
        tf.verticalLink = BlzCreateFrameByType("BACKDROP", "VerticalLink", self.Frames.box, "", 0)
        self.verticalLink[i+1] = tf.verticalLink
    end

    tf.availableImage = BlzCreateFrameByType("BACKDROP", "AvailableImg", self.Frames.box, "", 0)
    tf.mainButton = BlzCreateFrame("ScoreScreenBottomButtonTemplate", self.Frames.box, 0, 0)
    tf.mainImage = BlzGetFrameByName("ScoreScreenButtonBackdrop", 0)
    tf.tooltipBox = BlzCreateFrame("ListBoxWar3", tf.mainButton, 0, 0)
    tf.tooltipText = BlzCreateFrameByType("TEXT", "StandardInfoTextTemplate", tf.tooltipBox, "StandardInfoTextTemplate", 0)
    tf.counterImage = BlzCreateFrameByType("BACKDROP", "Counter", tf.mainButton, "", 0)
    tf.counterText = BlzCreateFrameByType("TEXT", "FaceFrameTooltip", tf.mainButton, "", 0)
    tf.tooltipRank = BlzCreateFrameByType("TEXT", "FaceFrameTooltip", tf.tooltipBox, "", 0)

    BlzFrameSetTooltip(tf.mainButton, tf.tooltipBox)
    BlzFrameSetTextAlignment(tf.counterText, TEXT_JUSTIFY_CENTER, TEXT_JUSTIFY_MIDDLE)
    BlzFrameSetTextAlignment(tf.tooltipRank, TEXT_JUSTIFY_TOP, TEXT_JUSTIFY_RIGHT)

    local xPos = math.floor(math.fmod(i, self.COLUMNS))
    local yPos = math.floor((i) / self.COLUMNS)

    local xIncrem = (self.WIDTH*(1-self.SIDE_MARGIN)) / (self.COLUMNS + 1)
    local yIncrem = (self.HEIGHT*(1-self.VERTICAL_MARGIN)) / (self.ROWS + 1)
    local xOffset = xPos * xIncrem - ((self.COLUMNS-1) * 0.5) * xIncrem
    --local yOffset = self.HEIGHT*(0.1+self.VERTICAL_MARGIN) + yPos * ((self.HEIGHT*(1-self.VERTICAL_MARGIN)) / self.ROWS)
    local yOffset = yPos * yIncrem - ((self.ROWS-1) * 0.5) * yIncrem

    local Config = {
        --mainButton = { point = true, pos = { fp = FRAMEPOINT_CENTER, frame = self.Frames.box, fp2 = FRAMEPOINT_BOTTOM, x = xOffset, y = yOffset }, size = { x = self.TALENT_WIDTH, y = self.TALENT_HEIGHT }},
        mainButton = { point = true, pos = { fp = FRAMEPOINT_CENTER, frame = self.Frames.box, fp2 = FRAMEPOINT_CENTER, x = xOffset, y = yOffset }, size = { x = self.TALENT_WIDTH, y = self.TALENT_HEIGHT }},
        tooltipBox = { point = true, pos = { fp = FRAMEPOINT_TOPLEFT, frame = self.Frames.box, fp2 = FRAMEPOINT_TOPRIGHT, x = 0.0, y = 0.0 }, size = { x = self.TOOLTIP_BOX_WIDTH, y = self.TOOLTIP_BOX_HEIGHT }},
        tooltipText = { clear = true, point = true, pos = { fp = FRAMEPOINT_CENTER, frame = tf.tooltipBox, fp2 = FRAMEPOINT_CENTER, x = 0.0, y = 0.0 }, size = { x = self.TOOLTIP_BOX_WIDTH-0.03, y = self.TOOLTIP_BOX_HEIGHT-0.03 }, text = "Default talent name \n\nDefault talent description"},
        counterImage = { point = true, pos = { fp = FRAMEPOINT_BOTTOMRIGHT, frame = tf.mainButton, fp2 = FRAMEPOINT_BOTTOMRIGHT, x = -0.0006, y = 0.0015 }, size = { x = 0.014, y = 0.014 }, texture = self.COUNTER_ICON_ACTIVE },
        counterText = { clear = true, point = true, pos = { fp = FRAMEPOINT_CENTER, frame = tf.counterImage, fp2 = FRAMEPOINT_CENTER, x = 0, y = 0}, size = { x = 0.01, y = 0.012 }, text = "0"},
        verticalLink = { point = true, pos = { fp = FRAMEPOINT_BOTTOM, frame = self.Frames.box, fp2 = FRAMEPOINT_CENTER, x = xOffset, y = yOffset }, size = { x = self.TALENT_WIDTH*0.10, y = yIncrem }, texture = self.INACTIVE_LINK_BOX },
        horizontalLink = { point = true, pos = { fp = FRAMEPOINT_LEFT, frame = self.Frames.box, fp2 = FRAMEPOINT_CENTER, x = xOffset, y = yOffset }, size = { x = xIncrem, y = self.TALENT_HEIGHT*0.10 }, texture = self.INACTIVE_LINK_BOX },
        availableImage = { point = true, pos = { fp = FRAMEPOINT_CENTER, frame = tf.mainButton, fp2 = FRAMEPOINT_CENTER, x = 0, y = 0 }, size = { x = self.TALENT_WIDTH*self.OUTLINE_BORDER_RATIO, y = self.TALENT_HEIGHT*self.OUTLINE_BORDER_RATIO }, texture = self.GREEN_BORDER },
        mainImage = { texture = self.DEFAULT_TALENT_ICON },
        tooltipRank = { clear = true, point = true, pos = { fp = FRAMEPOINT_TOP, frame = tf.tooltipBox, fp2 = FRAMEPOINT_TOP, x = 0.0, y = -0.015 }, size = { x = self.TOOLTIP_BOX_WIDTH-0.03, y = self.TOOLTIP_BOX_HEIGHT-0.03 }, text = "Rank 1/3"},
    }

    ConfigFrames(Config, tf)

    tf.onClick = function()
        local frame = BlzGetTriggerFrame()
        BlzFrameSetEnable(frame, false)
        BlzFrameSetEnable(frame, true)
        TalentView:OnClicked(i+1, GetTriggerPlayer())
    end

    TriggerAddAction(tf.clickTrigger, tf.onClick)
    BlzTriggerRegisterFrameEvent(tf.clickTrigger, tf.mainButton, FRAMEEVENT_CONTROL_CLICK)

    if (TalentView.RIGHT_CLICK_TALENT_REMOVAL) then
        tf.mouseUpTrigger = CreateTrigger()
        TriggerAddAction(tf.mouseUpTrigger, function()
            local owner = GetTriggerPlayer()
            if (TalentView.playerRightClick[owner] == true) then
                TalentView:OnRightClick(i+1, owner)
            end
        end)
        BlzTriggerRegisterFrameEvent(tf.mouseUpTrigger, tf.mainButton, FRAMEEVENT_MOUSE_UP)
    end

    return tf
end

function TalentView:Initialize()

    self.Frames = {}
    self.Config = {}
    self.MAX_TALENTS = self.COLUMNS * self.ROWS
    -- Create and setup the Talent View dialog box
    self.Frames.box  = BlzCreateFrame("SuspendDialog", BlzGetOriginFrame(ORIGIN_FRAME_GAME_UI,0), 0,0)
    self.Config.box = { clear = true, abs = true, pos = { x = 0.35, y = 0.34, fp = FRAMEPOINT_CENTER }, size = { x = self.WIDTH, y = self.HEIGHT }}
    self.Frames.boxTitle = BlzGetFrameByName("SuspendTitleText",0)
    self.Config.boxTitle = { text = "Talent Tree" }

    -- Create and set up the Talent Save/Confirmation Button Frame
    self.Frames.confirmButton  = BlzGetFrameByName("SuspendDropPlayersButton",0)
    self.Config.confirmButton = { clear = true, point = true, pos = { x = 0.0, y = 0.02, frame = self.Frames.box, fp = FRAMEPOINT_BOTTOMRIGHT, fp2 = FRAMEPOINT_BOTTOM }, size = { x = 0.12, y = 0.03 }}
    self.Frames.confirmText = BlzGetFrameByName("SuspendDropPlayersButtonText",0)
    self.Config.confirmText = { text = "Confirm" }

    -- Create and setup the Talent Cancel/Close view Button Frame
    self.Frames.cancelButton = BlzCreateFrame("ScriptDialogButton", self.Frames.box, 0,0)
    self.Config.cancelButton = { clear = true, point = true, pos = { x = 0.0, y = 0.02, frame = self.Frames.box, fp = FRAMEPOINT_BOTTOMLEFT, fp2 = FRAMEPOINT_BOTTOM }, size = { x = 0.12, y = 0.03 }}
    self.Frames.cancelText = BlzGetFrameByName("ScriptDialogButtonText",0)
    self.Config.cancelText = { text = "Cancel" }

    -- Create and setup the Show Talents Button Frame
    self.Frames.viewTalentsButton = BlzCreateFrame("ScriptDialogButton", BlzGetOriginFrame(ORIGIN_FRAME_GAME_UI,0), 0, 0)
    self.Config.viewTalentsButton = { clear = true, abs = true, pos = { x = self.SHOW_TALENTS_X, y = self.SHOW_TALENTS_Y, fp = FRAMEPOINT_CENTER }, size = { x = 0.11, y = 0.035 }}
    self.Frames.viewTalentsText = BlzGetFrameByName("ScriptDialogButtonText",0)
    self.Config.viewTalentsText = { text = "Show Talents" }

    -- Apply the config to all created frames.
    ConfigFrames(self.Config, self.Frames)

    -- Create the talent frames
    self.Frames.Talent = {}
    for i = 0, self.COLUMNS * self.ROWS - 1 do
        table.insert(self.Frames.Talent, TalentFrameInitialize(i))
    end

    -- Create the needed triggers for the buttons
    self.confirmTrigger = CreateTrigger()
    self.cancelTrigger = CreateTrigger()
    self.viewTrigger = CreateTrigger()

    -- Add the trigger Actions
    TriggerAddAction(self.confirmTrigger, self.OnConfirm)
    TriggerAddAction(self.cancelTrigger, self.OnCancel)
    TriggerAddAction(self.viewTrigger, function() TalentView:OnView(GetTriggerPlayer()) end)

    -- Register the event on the frames
    BlzTriggerRegisterFrameEvent(self.confirmTrigger, self.Frames.confirmButton, FRAMEEVENT_CONTROL_CLICK)
    BlzTriggerRegisterFrameEvent(self.cancelTrigger, self.Frames.cancelButton, FRAMEEVENT_CONTROL_CLICK)
    BlzTriggerRegisterFrameEvent(self.viewTrigger, self.Frames.viewTalentsButton, FRAMEEVENT_CONTROL_CLICK)

    if (self.RIGHT_CLICK_TALENT_REMOVAL) then
        self.setRightClickDownTrigger = CreateTrigger()
        self.unsetRightClickUpTrigger = CreateTrigger()
        TriggerRegisterPlayerMouseEventBJ(self.setRightClickDownTrigger, Player(0), bj_MOUSEEVENTTYPE_DOWN)
        TriggerRegisterPlayerMouseEventBJ(self.unsetRightClickUpTrigger, Player(0), bj_MOUSEEVENTTYPE_UP)
        TriggerAddAction(self.setRightClickDownTrigger, function()
            if (BlzGetTriggerPlayerMouseButton() == MOUSE_BUTTON_TYPE_RIGHT) then
                TalentView.playerRightClick[GetTriggerPlayer()] = true
            end
        end)
        TriggerAddAction(self.unsetRightClickUpTrigger, function()
            local player = GetTriggerPlayer()
            TimerStart(NewTimer(), 0.1, false, function() ReleaseTimer(GetExpiredTimer()) TalentView.playerRightClick[player] = false end)
        end)
    end

    BlzFrameSetVisible(self.Frames.box, false)
    BlzFrameSetVisible(self.Frames.viewTalentsButton, false)
end

function ConfigFrames(configs, frames)
    local keys = {}
    for n in pairs(configs) do table.insert(keys, n) end
    table.sort(keys)

    for i, k in ipairs(keys) do
        local v = configs[k]
        if (v.clear) then BlzFrameClearAllPoints(frames[k]) end
        if (v.size) then BlzFrameSetSize(frames[k], v.size.x, v.size.y) end
        if (v.abs) then BlzFrameSetAbsPoint(frames[k], v.pos.fp, v.pos.x, v.pos.y)
        elseif (v.point) then BlzFrameSetPoint(frames[k], v.pos.fp, v.pos.frame, v.pos.fp2, v.pos.x, v.pos.y) end
        if (v.text) then BlzFrameSetText(frames[k], v.text) end
        if (v.texture) then BlzFrameSetTexture(frames[k], v.texture, 0, true) end
    end
end

onGlobalInit(function()
    TimerStart(NewTimer(), 0.0, false, function() Test() end)
end)

-- THIS IS FOR TEST ONLY
function Test()
    TalentView:Initialize()
end