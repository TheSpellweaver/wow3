function SetupPartyProfiles()
    -- Condense the hero icons a bit
    local heroBar = BlzGetOriginFrame(ORIGIN_FRAME_HERO_BAR, 0)
    BlzFrameSetSize(heroBar, 0.038, 0.3)

    for i = 0, 11 do
        local hero = BlzGetOriginFrame(ORIGIN_FRAME_HERO_BUTTON, i)

        local hp = BlzGetOriginFrame(ORIGIN_FRAME_HERO_HP_BAR, i)
        local mp = BlzGetOriginFrame(ORIGIN_FRAME_HERO_MANA_BAR, i)

        BlzFrameClearAllPoints(hp)
        BlzFrameClearAllPoints(mp)

        BlzFrameSetPoint(hp, FRAMEPOINT_TOPLEFT, hero, FRAMEPOINT_TOPRIGHT, 0, -0.01)
        BlzFrameSetPoint(mp, FRAMEPOINT_BOTTOMLEFT, hero, FRAMEPOINT_BOTTOMRIGHT,  0, 0.002)

        BlzFrameSetPoint(hp, FRAMEPOINT_RIGHT, hero, FRAMEPOINT_RIGHT,       0.1, 0) -- 0.07
        BlzFrameSetPoint(hp, FRAMEPOINT_BOTTOMLEFT, hero, FRAMEPOINT_RIGHT,     0, -0.01) -- 0.07

        BlzFrameSetPoint(mp, FRAMEPOINT_RIGHT, hero, FRAMEPOINT_RIGHT, 0.1, 0)
        BlzFrameSetPoint(mp, FRAMEPOINT_TOPLEFT, hero, FRAMEPOINT_RIGHT, 0, -0.01) -- 0.07
    end
end

onGlobalInit(function()
    TimerStart(NewTimer(), 0.0, false, SetupPartyProfiles)
end)