onGlobalInit(function()
    local t = CreateTrigger()
    TriggerRegisterPlayerChatEvent(t, Player(0), "-lvl ", false)
    TriggerAddAction(t, function()
        local msg = GetEventPlayerChatString()
        local lvl = tonumber(SubString(msg, 5, 7))
        if (lvl > 0) then
            GroupEnumUnitsSelected(SpellGroup, Player(0), null)
            local u = FirstOfGroup(SpellGroup)
            local lvlDiff = lvl - GetHeroLevel(u)
            SetHeroLevel(u, lvl, true)
            if (lvlDiff > 0 and TalentTree[u] and TalentTree[u].pointsAvailable) then
                TalentTree[u].pointsAvailable = TalentTree[u].pointsAvailable + lvlDiff
            end
        end
    end)
end)