# Wow3

# 1. What is this
A Lua project that aims to make dungeon crawler maps with shared class/spells/systems/items, but different content (dungeons). Currently based on World of Warcraft Cataclysm.

# 2. How to set up
This is built using [Ceres](https://github.com/ElusiveMori/ceres-wc3), a Lua external build tool. It requires folder structure which is why the example project is one as well.

The best IDE seems to be VSCode so here will be explained how to set that up.
- Download VS Code
- Follow [TriggerHappy's instructions](https://www.hiveworkshop.com/threads/lua-vscode-integration.314974/) on how to set up VSCode for wc3

# 3. Run the example
- Run ceres.exe
- Open the cmd terminal in VSCode (Shift+tilda), if not set it up to cmd